package controller;





import model.*;

public class Giocatore {
	DatiConnessione connessione;
	CartaPersonaggio personaggio;
	boolean vittoria;
	boolean morte;
	
	public DatiConnessione getConnessione() {
		return connessione;
	}
	public void setConnessione(DatiConnessione connessione) {
		this.connessione = connessione;
	}
	public CartaPersonaggio getPersonaggio() {
		return personaggio;
	}
	public void setPersonaggio(CartaPersonaggio personaggio) {
		this.personaggio = personaggio;
	}
	public boolean isVittoria() {
		return vittoria;
	}
	public void setVittoria(boolean vittoria) {
		this.vittoria = vittoria;
	}
	public boolean isMorte() {
		return morte;
	}
	public void setMorte(boolean morte) {
		this.morte = morte;
	}
	
	
}
