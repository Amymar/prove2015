package controller;




import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import model.DatiPartita;

public class Partita implements Runnable {
	private Map <String, Giocatore> giocatori= new TreeMap <String, Giocatore>();
	long idPartita;
	int turno;
	DatiPartita dati;
	
	public Partita (){
		idPartita= new Date().getTime();
	}

	public void run(){
		// TODO Auto-generated method stub
		assegnaPersonaggi();

	}

	private void assegnaPersonaggi(){
		Map <String, Giocatore> copia= new TreeMap <String, Giocatore>();
		copia=giocatori.clone();
		Random randomGiocatore = new Random();
		Random randomUmani= new Random();
		Random randomAlieni= new Random();
		List<String> keysGiocatore = new ArrayList<String>(giocatori.keySet());
		List<String> keysUmani = new ArrayList<String>(getDati().getUmani().keySet());
		List<String> keysAlieni = new ArrayList<String>(getDati().getAlieni().keySet());			
		String randomKeyGiocatore;
		String randomKeyUmani;
		String randomKeyAlieni;

		for(int i=0; i<giocatori.size(); i++){
			while (!copia.isEmpty()){
				for(Razza razza = Razza.values()){
					if(razza=="ALIEN"){
						randomKeyGiocatore=keysGiocatore.get( randomGiocatore.nextInt(keysGiocatore.size()) );
						randomKeyAlieni= keysAlieni.get( randomAlieni.nextInt(keysAlieni.size()) );
						getGiocatori().get(randomKeyGiocatore).setPersonaggio(getDati().getAlieni().get(randomKeyAlieni);
						keysGiocatore.remove(randomKeyGiocatore);
						keysAlieni.remove(randomKeyAlieni);
						copia.remove(randomKeyGiocatore);

					}else if (razza=="HUMAN"){
						randomKeyGiocatore=keysGiocatore.get( randomGiocatore.nextInt(keysGiocatore.size()) );
						randomKeyUmani= keysUmani.get( randomUmani.nextInt(keysUmani.size()) );
						getGiocatori().get(randomKeyGiocatore).setPersonaggio(getDati().getUmani().get(randomKeyUmani);
						keysGiocatore.remove(randomKeyGiocatore);
						keysUmani.remove(randomKeyUmani);
						copia.remove(randomKeyGiocatore); 
					}
				}
			}
		}
	}

	private void gestoreTurni(){
		while (getTurno()<40){
			for(String key: Giocatori.keySet()){
				//TODO completare

			}	

	
}


	public Map<String, Giocatore> getGiocatori() {
		return giocatori;
	}

	public void setGiocatori(Map<String, Giocatore> giocatori) {
		this.giocatori = giocatori;
	}

	public long getIdPartita() {
		return idPartita;
	}

	public void setIdPartita(long idPartita) {
		this.idPartita = idPartita;
	}

	public int getTurno() {
		return turno;
	}

	public void setTurno(int turno) {
		this.turno = turno;
	}

	public DatiPartita getDati() {
		return dati;
	}

	public void setDati(DatiPartita dati) {
		this.dati = dati;
	}