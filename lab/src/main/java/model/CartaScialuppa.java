package model;

import model.Enumerazioni.*;


public class CartaScialuppa {
	private String id;
	private Colore colore;
	
	public CartaScialuppa (String id, Colore colore){
		this.id=id;
		this.colore=colore;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Colore getColore() {
		return colore;
	}

	public void setColore(Colore colore) {
		this.colore = colore;
	}

	
}
