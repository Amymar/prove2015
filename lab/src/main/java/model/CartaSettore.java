package model;

import model.Enumerazioni.*;

public class CartaSettore {
	
	private String id;
	private CSettore tipo;
	private boolean oggetto;
	
	public CartaSettore (String id, CSettore tipo, boolean oggetto){
		this.id=id;
		this.tipo=tipo;
		this.oggetto=oggetto;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CSettore getTipo() {
		return tipo;
	}

	public void setTipo(CSettore tipo) {
		this.tipo = tipo;
	}

	public boolean isOggetto() {
		return oggetto;
	}

	public void setOggetto(boolean oggetto) {
		this.oggetto = oggetto;
	}
	
	
}