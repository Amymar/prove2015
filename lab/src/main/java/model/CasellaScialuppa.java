package model;

import java.util.Map;

import model.Enumerazioni.*;

public class CasellaScialuppa extends Casella {
	private boolean open;
	
	public CasellaScialuppa(String nome, TipoTerreno tipo, boolean open){
		super (nome,tipo);
		this.open=open;
	}
	
	public static Casella getScialuppa(String IdUnivoco, TipoTerreno tipo, Map <String, Casella> caselle){
		if(caselle.containsKey(IdUnivoco)){
			return caselle.get(IdUnivoco);
		}else{
			Casella casella = new CasellaScialuppa(IdUnivoco,tipo,true);
			caselle.put(IdUnivoco, casella);
			return casella;
		}
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}
	
}
