package model;

import java.util.Map;
import java.util.TreeMap;
import model.Enumerazioni.*;


public class Mappa {

	private Map <String, Casella> settori;
//	private Map <String, CartaScialuppa> carteScialuppa;
//	private Map <String, CartaSettore> carteSettore;
	
	public Mappa (){
		
		this.settori=new TreeMap <String, Casella>();
//		this.carteScialuppa= new TreeMap <String, CartaScialuppa>();
//		this.carteSettore= new TreeMap<String , CartaSettore>();
		
		//Scialuppe
		Casella scialuppa1=CasellaScialuppa.getScialuppa("Z01", TipoTerreno.BOAT, settori);
		Casella scialuppa2=CasellaScialuppa.getScialuppa("Z02", TipoTerreno.BOAT, settori);
		Casella scialuppa3=CasellaScialuppa.getScialuppa("Z03", TipoTerreno.BOAT, settori);
		Casella scialuppa4=CasellaScialuppa.getScialuppa("Z04", TipoTerreno.BOAT, settori);
		
		//basi
		Casella baseUmani= Casella.getCasella ("XU", TipoTerreno.HUMANS, getSettori());
		Casella baseAlieni= Casella.getCasella ("XA", TipoTerreno.ALIENS, getSettori());


		
		//caselle A
		Casella a2= Casella.getCasella ("A02", TipoTerreno.DANGER, getSettori());
		Casella a3=Casella.getCasella ("A03", TipoTerreno.DANGER, getSettori());
		Casella a4=Casella.getCasella ("A04", TipoTerreno.SAFE, getSettori());
		Casella a5=Casella.getCasella ("A05", TipoTerreno.SAFE, getSettori());
		Casella a6=Casella.getCasella ("A06", TipoTerreno.SAFE, getSettori());
		Casella a9=Casella.getCasella ("A09", TipoTerreno.SAFE, getSettori());
		Casella a10=Casella.getCasella ("A10", TipoTerreno.SAFE, getSettori());
		Casella a11=Casella.getCasella ("A11", TipoTerreno.SAFE, getSettori());
		Casella a12=Casella.getCasella ("A12", TipoTerreno.SAFE, getSettori());
		Casella a13=Casella.getCasella("A13", TipoTerreno.SAFE, getSettori());
		Casella a14=Casella.getCasella ("A14", TipoTerreno.DANGER, getSettori());
		
		//caselle B
		Casella b1= Casella.getCasella ("B01", TipoTerreno.DANGER, getSettori());
		Casella b3= Casella.getCasella ("B03", TipoTerreno.DANGER, getSettori());
		Casella b4= Casella.getCasella ("B04", TipoTerreno.DANGER, getSettori());
		Casella b5= Casella.getCasella ("B05", TipoTerreno.SAFE, getSettori());
		Casella b6= Casella.getCasella ("B06", TipoTerreno.DANGER, getSettori());
		Casella b8= Casella.getCasella ("B08", TipoTerreno.DANGER, getSettori());
		Casella b9= Casella.getCasella ("B09", TipoTerreno.DANGER, getSettori());
		Casella b10= Casella.getCasella ("B10", TipoTerreno.SAFE, getSettori());
		Casella b11= Casella.getCasella ("B11", TipoTerreno.DANGER, getSettori());
		Casella b12= Casella.getCasella ("B12", TipoTerreno.DANGER, getSettori());
		Casella b14= Casella.getCasella ("B14", TipoTerreno.DANGER, getSettori());
		
		//caselle C
		Casella c1= Casella.getCasella ("C01", TipoTerreno.SAFE, getSettori());
		Casella c2= Casella.getCasella ("C02", TipoTerreno.DANGER, getSettori());
		Casella c3= Casella.getCasella ("C03", TipoTerreno.DANGER, getSettori());
		Casella c4= Casella.getCasella ("C04", TipoTerreno.DANGER, getSettori());
		Casella c5= Casella.getCasella ("C05", TipoTerreno.DANGER, getSettori());
		Casella c6= Casella.getCasella ("C06", TipoTerreno.DANGER, getSettori());
		Casella c7= Casella.getCasella ("C07", TipoTerreno.DANGER, getSettori());
		Casella c8= Casella.getCasella ("C08", TipoTerreno.DANGER, getSettori());
		Casella c9= Casella.getCasella ("C09", TipoTerreno.DANGER, getSettori());
		Casella c10= Casella.getCasella ("C10", TipoTerreno.DANGER, getSettori());
		Casella c11= Casella.getCasella ("C11", TipoTerreno.DANGER, getSettori());
		Casella c12= Casella.getCasella ("C12", TipoTerreno.DANGER, getSettori());
		Casella c13= Casella.getCasella ("C13", TipoTerreno.DANGER, getSettori());
		Casella c14= Casella.getCasella ("C14", TipoTerreno.SAFE, getSettori());
		
		//caselle D
		Casella d2= Casella.getCasella ("D02", TipoTerreno.DANGER, getSettori());
		Casella d3= Casella.getCasella ("D03", TipoTerreno.DANGER, getSettori());
		Casella d5= Casella.getCasella ("D05", TipoTerreno.DANGER, getSettori());
		Casella d8= Casella.getCasella ("D08", TipoTerreno.DANGER, getSettori());
		Casella d9= Casella.getCasella ("D09", TipoTerreno.DANGER, getSettori());
		Casella d10= Casella.getCasella ("D10", TipoTerreno.SAFE, getSettori());
		Casella d11= Casella.getCasella ("D11", TipoTerreno.DANGER, getSettori());
		Casella d12= Casella.getCasella ("D12", TipoTerreno.DANGER, getSettori());
		Casella d13= Casella.getCasella ("D13", TipoTerreno.DANGER, getSettori());
		Casella d14= Casella.getCasella ("D14", TipoTerreno.SAFE, getSettori());
		
		//caselle E
		Casella e2= Casella.getCasella ("E02", TipoTerreno.SAFE, getSettori());
		Casella e3= Casella.getCasella ("E03", TipoTerreno.DANGER, getSettori());
		Casella e4= Casella.getCasella ("E04", TipoTerreno.DANGER, getSettori());
		Casella e5= Casella.getCasella ("E05", TipoTerreno.DANGER, getSettori());
		Casella e6= Casella.getCasella ("E06", TipoTerreno.DANGER, getSettori());
		Casella e8= Casella.getCasella ("E08", TipoTerreno.DANGER, getSettori());
		Casella e9= Casella.getCasella ("E09", TipoTerreno.DANGER, getSettori());
		Casella e10= Casella.getCasella ("E10", TipoTerreno.DANGER, getSettori());
		Casella e11= Casella.getCasella ("E11", TipoTerreno.DANGER, getSettori());
		Casella e12= Casella.getCasella ("E12", TipoTerreno.SAFE, getSettori());
		Casella e13= Casella.getCasella ("E13", TipoTerreno.DANGER, getSettori());
		
		//caselle F
		Casella f1= Casella.getCasella ("F01", TipoTerreno.SAFE, getSettori());
		Casella f2= Casella.getCasella ("F02", TipoTerreno.DANGER, getSettori());
		Casella f3= Casella.getCasella ("F03", TipoTerreno.DANGER, getSettori());
		Casella f4= Casella.getCasella ("F04", TipoTerreno.DANGER, getSettori());
		Casella f5= Casella.getCasella ("F05", TipoTerreno.DANGER, getSettori());
		Casella f6= Casella.getCasella ("F06", TipoTerreno.DANGER, getSettori());
		Casella f7= Casella.getCasella ("F07", TipoTerreno.DANGER, getSettori());
		Casella f8= Casella.getCasella ("F08", TipoTerreno.DANGER, getSettori());
		Casella f9= Casella.getCasella ("F09", TipoTerreno.DANGER, getSettori());
		Casella f10= Casella.getCasella ("F10", TipoTerreno.SAFE, getSettori());
		Casella f11= Casella.getCasella ("F11", TipoTerreno.DANGER, getSettori());
		
		//caselle G
		Casella g1= Casella.getCasella ("G01", TipoTerreno.DANGER, getSettori());
		Casella g2= Casella.getCasella ("G02", TipoTerreno.DANGER, getSettori());
		Casella g3= Casella.getCasella ("G03", TipoTerreno.DANGER, getSettori());
		Casella g4= Casella.getCasella ("G04", TipoTerreno.DANGER, getSettori());
		Casella g5= Casella.getCasella ("G05", TipoTerreno.DANGER, getSettori());
		Casella g6= Casella.getCasella ("G06", TipoTerreno.DANGER, getSettori());
		Casella g7= Casella.getCasella ("G07", TipoTerreno.SAFE, getSettori());
		Casella g8= Casella.getCasella ("G08", TipoTerreno.DANGER, getSettori());
		Casella g9= Casella.getCasella ("G09", TipoTerreno.DANGER, getSettori());
		Casella g10= Casella.getCasella ("G10", TipoTerreno.DANGER, getSettori());
		Casella g11= Casella.getCasella ("G11", TipoTerreno.DANGER, getSettori());
		Casella g12= Casella.getCasella ("G12", TipoTerreno.SAFE, getSettori());
		Casella g13= Casella.getCasella ("G13", TipoTerreno.DANGER, getSettori());
		Casella g14= Casella.getCasella ("G14", TipoTerreno.SAFE, getSettori());
		
		//caselle H
		Casella h1= Casella.getCasella ("H01", TipoTerreno.SAFE, getSettori());
		Casella h2= Casella.getCasella ("H02", TipoTerreno.SAFE, getSettori());
		Casella h3= Casella.getCasella ("H03", TipoTerreno.SAFE, getSettori());
		Casella h4= Casella.getCasella ("H04", TipoTerreno.DANGER, getSettori());
		Casella h6= Casella.getCasella ("H06", TipoTerreno.DANGER, getSettori());
		Casella h7= Casella.getCasella ("H07", TipoTerreno.SAFE, getSettori());
		Casella h8= Casella.getCasella ("H08", TipoTerreno.DANGER, getSettori());
		Casella h9= Casella.getCasella ("H09", TipoTerreno.DANGER, getSettori());
		Casella h12= Casella.getCasella ("H12", TipoTerreno.DANGER, getSettori());
		Casella h13= Casella.getCasella ("H13", TipoTerreno.DANGER, getSettori());
		Casella h14= Casella.getCasella ("H14", TipoTerreno.SAFE, getSettori());
		
		//caselle I
		Casella i1= Casella.getCasella ("I01", TipoTerreno.SAFE, getSettori());
		Casella i2= Casella.getCasella ("I02", TipoTerreno.DANGER, getSettori());
		Casella i3= Casella.getCasella ("I03", TipoTerreno.DANGER, getSettori());
		Casella i4= Casella.getCasella ("I04", TipoTerreno.DANGER, getSettori());
		Casella i5= Casella.getCasella ("I05", TipoTerreno.DANGER, getSettori());
		Casella i7= Casella.getCasella ("I07", TipoTerreno.DANGER, getSettori());
		Casella i8= Casella.getCasella ("I08", TipoTerreno.DANGER, getSettori());
		Casella i9= Casella.getCasella ("I09", TipoTerreno.SAFE, getSettori());
		Casella i10= Casella.getCasella ("I10", TipoTerreno.DANGER, getSettori());
		Casella i11= Casella.getCasella ("I11", TipoTerreno.DANGER, getSettori());
		Casella i13= Casella.getCasella ("I13", TipoTerreno.DANGER, getSettori());
		Casella i14= Casella.getCasella ("I14", TipoTerreno.SAFE, getSettori());
		
		//caselle J
		Casella j1= Casella.getCasella ("J01", TipoTerreno.SAFE, getSettori());
		Casella j2= Casella.getCasella ("J02", TipoTerreno.DANGER, getSettori());
		Casella j3= Casella.getCasella ("J03", TipoTerreno.DANGER, getSettori());
		Casella j4= Casella.getCasella ("J04", TipoTerreno.DANGER, getSettori());
		Casella j5= Casella.getCasella ("J05", TipoTerreno.DANGER, getSettori());
		Casella j6= Casella.getCasella ("J06", TipoTerreno.DANGER, getSettori());
		Casella j8= Casella.getCasella ("J08", TipoTerreno.DANGER, getSettori());
		Casella j9= Casella.getCasella ("J09", TipoTerreno.DANGER, getSettori());
		Casella j10= Casella.getCasella ("J10", TipoTerreno.DANGER, getSettori());
		Casella j11= Casella.getCasella ("J11", TipoTerreno.DANGER, getSettori());
		Casella j13= Casella.getCasella ("J13", TipoTerreno.DANGER, getSettori());
		Casella j14= Casella.getCasella ("J14", TipoTerreno.SAFE, getSettori());

		// caselle K
		Casella k1= Casella.getCasella ("K01", TipoTerreno.DANGER, getSettori());
		Casella k2= Casella.getCasella ("K02", TipoTerreno.SAFE, getSettori());
		Casella k3= Casella.getCasella ("K03", TipoTerreno.DANGER, getSettori());
		Casella k4= Casella.getCasella ("K04", TipoTerreno.DANGER, getSettori());
		Casella k5= Casella.getCasella ("K05", TipoTerreno.SAFE, getSettori());
		Casella k6= Casella.getCasella ("K06", TipoTerreno.DANGER, getSettori());
		Casella k8= Casella.getCasella ("K08", TipoTerreno.DANGER, getSettori());
		Casella k9= Casella.getCasella ("K09", TipoTerreno.SAFE, getSettori());
		Casella k10= Casella.getCasella ("K10", TipoTerreno.DANGER, getSettori());
		Casella k11= Casella.getCasella ("K11", TipoTerreno.SAFE, getSettori());
		Casella k12= Casella.getCasella ("K12", TipoTerreno.DANGER, getSettori());
		Casella k14= Casella.getCasella ("K14", TipoTerreno.DANGER, getSettori());
		
		//casselle L
		Casella l1= Casella.getCasella ("L01", TipoTerreno.DANGER, getSettori());
		Casella l2= Casella.getCasella ("L02", TipoTerreno.SAFE, getSettori());
		Casella l3= Casella.getCasella ("L03", TipoTerreno.DANGER, getSettori());
		Casella l4= Casella.getCasella ("L04", TipoTerreno.SAFE, getSettori());
		Casella l5= Casella.getCasella ("L05", TipoTerreno.DANGER, getSettori());
		Casella l9= Casella.getCasella ("L09", TipoTerreno.SAFE, getSettori());
		Casella l10= Casella.getCasella ("L10", TipoTerreno.DANGER, getSettori());
		Casella l11= Casella.getCasella ("L11", TipoTerreno.SAFE, getSettori());
		Casella l12= Casella.getCasella ("L12", TipoTerreno.DANGER, getSettori());
		Casella l13= Casella.getCasella ("L13", TipoTerreno.DANGER, getSettori());
		Casella l14= Casella.getCasella ("L14", TipoTerreno.SAFE, getSettori());
		
		//caselle M
		Casella m1= Casella.getCasella ("M01", TipoTerreno.DANGER, getSettori());
		Casella m2= Casella.getCasella ("M02", TipoTerreno.SAFE, getSettori());
		Casella m3= Casella.getCasella ("M03", TipoTerreno.DANGER, getSettori());
		Casella m4= Casella.getCasella ("M04", TipoTerreno.DANGER, getSettori());
		Casella m5= Casella.getCasella ("M05", TipoTerreno.SAFE, getSettori());
		Casella m6= Casella.getCasella ("M06", TipoTerreno.DANGER, getSettori());
		Casella m8= Casella.getCasella ("M08", TipoTerreno.DANGER, getSettori());
		Casella m9= Casella.getCasella ("M09", TipoTerreno.SAFE, getSettori());
		Casella m10= Casella.getCasella ("M10", TipoTerreno.DANGER, getSettori());
		Casella m11= Casella.getCasella ("M11", TipoTerreno.SAFE, getSettori());
		Casella m12= Casella.getCasella ("M12", TipoTerreno.DANGER, getSettori());
		Casella m13= Casella.getCasella ("M13", TipoTerreno.DANGER, getSettori());
		Casella m14= Casella.getCasella ("M14", TipoTerreno.DANGER, getSettori());
		
		//caselle N
		Casella n1= Casella.getCasella ("N01", TipoTerreno.DANGER, getSettori());
		Casella n2= Casella.getCasella ("N02", TipoTerreno.DANGER, getSettori());
		Casella n3= Casella.getCasella ("N03", TipoTerreno.SAFE, getSettori());
		Casella n4= Casella.getCasella ("N04", TipoTerreno.DANGER, getSettori());
		Casella n5= Casella.getCasella ("N05", TipoTerreno.DANGER, getSettori());
		Casella n6= Casella.getCasella ("N06", TipoTerreno.DANGER, getSettori());
		Casella n8= Casella.getCasella ("N08", TipoTerreno.DANGER, getSettori());
		Casella n9= Casella.getCasella ("N09", TipoTerreno.DANGER, getSettori());
		Casella n10= Casella.getCasella ("N10", TipoTerreno.DANGER, getSettori());
		Casella n11= Casella.getCasella ("N11", TipoTerreno.DANGER, getSettori());
		Casella n12= Casella.getCasella ("N12", TipoTerreno.DANGER, getSettori());
		Casella n13= Casella.getCasella ("N13", TipoTerreno.DANGER, getSettori());
		Casella n14= Casella.getCasella ("N14", TipoTerreno.SAFE, getSettori());
		
		//caselle O
		Casella o2= Casella.getCasella ("O02", TipoTerreno.DANGER, getSettori());
		Casella o5= Casella.getCasella ("O05", TipoTerreno.SAFE, getSettori());
		Casella o6= Casella.getCasella ("O06", TipoTerreno.DANGER, getSettori());
		Casella o7= Casella.getCasella ("O07", TipoTerreno.DANGER, getSettori());
		Casella o8= Casella.getCasella ("O08", TipoTerreno.DANGER, getSettori());
		Casella o9= Casella.getCasella ("O09", TipoTerreno.SAFE, getSettori());
		Casella o10= Casella.getCasella ("O10", TipoTerreno.DANGER, getSettori());
		Casella o11= Casella.getCasella ("O11", TipoTerreno.DANGER, getSettori());
		Casella o12= Casella.getCasella ("O12", TipoTerreno.DANGER, getSettori());
		Casella o13= Casella.getCasella ("O13", TipoTerreno.DANGER, getSettori());
		Casella o14= Casella.getCasella ("O14", TipoTerreno.SAFE, getSettori());
		
		//caselle P
		Casella p1= Casella.getCasella ("P01", TipoTerreno.SAFE, getSettori());
		Casella p2= Casella.getCasella ("P02", TipoTerreno.DANGER, getSettori());
		Casella p3= Casella.getCasella ("P03", TipoTerreno.SAFE, getSettori());
		Casella p4= Casella.getCasella ("P04", TipoTerreno.SAFE, getSettori());
		Casella p5= Casella.getCasella ("P05", TipoTerreno.DANGER, getSettori());
		Casella p6= Casella.getCasella ("P06", TipoTerreno.DANGER, getSettori());
		Casella p7= Casella.getCasella ("P07", TipoTerreno.DANGER, getSettori());
		Casella p8= Casella.getCasella ("P08", TipoTerreno.DANGER, getSettori());
		Casella p9= Casella.getCasella ("P09", TipoTerreno.DANGER, getSettori());
		Casella p10= Casella.getCasella ("P10", TipoTerreno.DANGER, getSettori());
		Casella p11= Casella.getCasella ("P11", TipoTerreno.DANGER, getSettori());
		Casella p12= Casella.getCasella ("P12", TipoTerreno.SAFE, getSettori());
		Casella p13= Casella.getCasella ("P13", TipoTerreno.DANGER, getSettori());
		Casella p14= Casella.getCasella ("P14", TipoTerreno.DANGER, getSettori());
		
		//caselle Q
		Casella	q1	=	Casella.getCasella("Q01	",TipoTerreno.SAFE,getSettori());
		Casella	q2	=	Casella.getCasella("Q02	",TipoTerreno.DANGER,getSettori());
		Casella	q3	=	Casella.getCasella("Q03	",TipoTerreno.DANGER,getSettori());
		Casella	q4	=	Casella.getCasella("Q04	",TipoTerreno.SAFE,getSettori());
		Casella	q5	=	Casella.getCasella("Q05	",TipoTerreno.DANGER,getSettori());
		Casella	q6	=	Casella.getCasella("Q06	",TipoTerreno.SAFE,getSettori());
		Casella	q7	=	Casella.getCasella("Q07	",TipoTerreno.DANGER,getSettori());
		Casella	q8	=	Casella.getCasella("Q08	",TipoTerreno.DANGER,getSettori());
		Casella	q9	=	Casella.getCasella("Q09	",TipoTerreno.DANGER,getSettori());
		Casella	q10	=	Casella.getCasella("Q10	",TipoTerreno.DANGER,getSettori());
		Casella	q11	=	Casella.getCasella("Q11	",TipoTerreno.SAFE,getSettori());
		Casella	q12	=	Casella.getCasella("Q12	",TipoTerreno.DANGER,getSettori());
		Casella	q13	=	Casella.getCasella("Q13	",TipoTerreno.DANGER,getSettori());
		Casella	q14	=	Casella.getCasella("Q14	",TipoTerreno.SAFE,getSettori());
		
		//caselle R
		Casella	r1	=	Casella.getCasella("R01	",TipoTerreno.SAFE,getSettori());
		Casella	r2	=	Casella.getCasella("R02	",TipoTerreno.DANGER,getSettori());
		Casella	r3	=	Casella.getCasella("R03	",TipoTerreno.DANGER,getSettori());
		Casella	r4	=	Casella.getCasella("R04	",TipoTerreno.SAFE,getSettori());
		Casella	r5	=	Casella.getCasella("R05	",TipoTerreno.DANGER,getSettori());
		Casella	r6	=	Casella.getCasella("R06	",TipoTerreno.SAFE,getSettori());
		Casella	r7	=	Casella.getCasella("R07	",TipoTerreno.SAFE,getSettori());
		Casella	r8	=	Casella.getCasella("R08	",TipoTerreno.SAFE,getSettori());
		Casella	r9	=	Casella.getCasella("R09	",TipoTerreno.DANGER,getSettori());
		Casella	r12	=	Casella.getCasella("R12	",TipoTerreno.SAFE,getSettori());
		Casella	r13	=	Casella.getCasella("R13	",TipoTerreno.DANGER,getSettori());
		
		//caselle S
		Casella	s2	=	Casella.getCasella("S02	",TipoTerreno.DANGER,getSettori());
		Casella	s4	=	Casella.getCasella("S04	",TipoTerreno.DANGER,getSettori());
		Casella	s5	=	Casella.getCasella("S05	",TipoTerreno.DANGER,getSettori());
		Casella	s6	=	Casella.getCasella("S06	",TipoTerreno.DANGER,getSettori());
		Casella	s7	=	Casella.getCasella("S07	",TipoTerreno.DANGER,getSettori());
		Casella	s8	=	Casella.getCasella("S08	",TipoTerreno.DANGER,getSettori());
		Casella	s9	=	Casella.getCasella("S09	",TipoTerreno.DANGER,getSettori());
		Casella	s12	=	Casella.getCasella("S12	",TipoTerreno.DANGER,getSettori());
		Casella	s13	=	Casella.getCasella("S13	",TipoTerreno.DANGER,getSettori());
		
		//caselle T
		Casella	t2	=	Casella.getCasella("T02	",TipoTerreno.DANGER,getSettori());
		Casella	t5	=	Casella.getCasella("T05	",TipoTerreno.DANGER,getSettori());
		Casella	t6	=	Casella.getCasella("T06	",TipoTerreno.DANGER,getSettori());
		Casella	t7	=	Casella.getCasella("T07	",TipoTerreno.SAFE,getSettori());
		Casella	t8	=	Casella.getCasella("T08	",TipoTerreno.SAFE,getSettori());
		Casella	t11	=	Casella.getCasella("T11	",TipoTerreno.DANGER,getSettori());
		Casella	t12	=	Casella.getCasella("T12	",TipoTerreno.DANGER,getSettori());
		Casella	t13	=	Casella.getCasella("T13	",TipoTerreno.DANGER,getSettori());
		Casella	t14	=	Casella.getCasella("T14	",TipoTerreno.SAFE,getSettori());
		
		//caselle U
		Casella	u1	=	Casella.getCasella("U01	",TipoTerreno.SAFE,getSettori());
		Casella	u2	=	Casella.getCasella("U02	",TipoTerreno.DANGER,getSettori());
		Casella	u3	=	Casella.getCasella("U03	",TipoTerreno.DANGER,getSettori());
		Casella	u4	=	Casella.getCasella("U04	",TipoTerreno.DANGER,getSettori());
		Casella	u5	=	Casella.getCasella("U05	",TipoTerreno.SAFE,getSettori());
		Casella	u6	=	Casella.getCasella("U06	",TipoTerreno.DANGER,getSettori());
		Casella	u7	=	Casella.getCasella("U07	",TipoTerreno.DANGER,getSettori());
		Casella	u8	=	Casella.getCasella("U08	",TipoTerreno.DANGER,getSettori());
		Casella	u9	=	Casella.getCasella("U09	",TipoTerreno.DANGER,getSettori());
		Casella	u10	=	Casella.getCasella("U10	",TipoTerreno.DANGER,getSettori());
		Casella	u11	=	Casella.getCasella("U11	",TipoTerreno.DANGER,getSettori());
		Casella	u12	=	Casella.getCasella("U12	",TipoTerreno.SAFE,getSettori());
		Casella	u13	=	Casella.getCasella("U13	",TipoTerreno.DANGER,getSettori());
		Casella	u14	=	Casella.getCasella("U14	",TipoTerreno.DANGER,getSettori());
		
		//caselle V
		Casella	v1	=	Casella.getCasella("V01	",TipoTerreno.SAFE,getSettori());
		Casella	v3	=	Casella.getCasella("V03	",TipoTerreno.DANGER,getSettori());
		Casella	v4	=	Casella.getCasella("V04	",TipoTerreno.DANGER,getSettori());
		Casella	v5	=	Casella.getCasella("V05	",TipoTerreno.DANGER,getSettori());
		Casella	v6	=	Casella.getCasella("V06	",TipoTerreno.DANGER,getSettori());
		Casella	v8	=	Casella.getCasella("V08	",TipoTerreno.SAFE,getSettori());
		Casella	v9	=	Casella.getCasella("V09	",TipoTerreno.DANGER,getSettori());
		Casella	v10	=	Casella.getCasella("V10	",TipoTerreno.DANGER,getSettori());
		Casella	v11	=	Casella.getCasella("V11	",TipoTerreno.DANGER,getSettori());
		Casella	v12	=	Casella.getCasella("V12	",TipoTerreno.DANGER,getSettori());
		Casella	v14	=	Casella.getCasella("V14	",TipoTerreno.DANGER,getSettori());
		
		//caselle W
		Casella	w2	=	Casella.getCasella("W02	",TipoTerreno.DANGER,getSettori());
		Casella	w3	=	Casella.getCasella("W03	",TipoTerreno.SAFE,getSettori());
		Casella	w4	=	Casella.getCasella("W04	",TipoTerreno.SAFE,getSettori());
		Casella	w5	=	Casella.getCasella("W05	",TipoTerreno.SAFE,getSettori());
		Casella	w6	=	Casella.getCasella("W06	",TipoTerreno.SAFE,getSettori());
		Casella	w9	=	Casella.getCasella("W09	",TipoTerreno.DANGER,getSettori());
		Casella	w10	=	Casella.getCasella("W10	",TipoTerreno.SAFE,getSettori());
		Casella	w11	=	Casella.getCasella("W11	",TipoTerreno.SAFE,getSettori());
		Casella	w12	=	Casella.getCasella("W12	",TipoTerreno.SAFE,getSettori());
		Casella	w13	=	Casella.getCasella("W13	",TipoTerreno.DANGER,getSettori());
		Casella	w14	=	Casella.getCasella("W14	",TipoTerreno.DANGER,getSettori());


		
		//adiacenti A
		a2.getSettoriAdiacenti().put("B01", b1);
		a2.getSettoriAdiacenti().put("Z01", scialuppa1);
		a2.getSettoriAdiacenti().put("A03", a3);
		
		a3.getSettoriAdiacenti().put("Z01", scialuppa1);
		a3.getSettoriAdiacenti().put("B03", b3);
		a3.getSettoriAdiacenti().put("A04", a4);
		a3.getSettoriAdiacenti().put("A02",	a2);
		
		a4.getSettoriAdiacenti().put("B03", b3);
		a4.getSettoriAdiacenti().put("B04", b4);
		a4.getSettoriAdiacenti().put("A05", a5);
		a4.getSettoriAdiacenti().put("A03",	a3);
		
		a5.getSettoriAdiacenti().put("A06", a6);
		a5.getSettoriAdiacenti().put("B04", b4);
		a5.getSettoriAdiacenti().put("B05", b5);
		a5.getSettoriAdiacenti().put("A04",	a4);
		
		a6.getSettoriAdiacenti().put("A05",	a5);
		a6.getSettoriAdiacenti().put("B05",	b5);
		a6.getSettoriAdiacenti().put("B06",	b6);
		
		a9.getSettoriAdiacenti().put("B08	",	b8);
		a9.getSettoriAdiacenti().put("B09	",	b9);
		a9.getSettoriAdiacenti().put("A10	",	a10);

		a10.getSettoriAdiacenti().put("A09	",	a9);
		a10.getSettoriAdiacenti().put("B09	",	b9);
		a10.getSettoriAdiacenti().put("B10	",	b10);
		a10.getSettoriAdiacenti().put("A11	",	a11);

		a11.getSettoriAdiacenti().put("A10	",	a10);
		a11.getSettoriAdiacenti().put("B10	",	b10);
		a11.getSettoriAdiacenti().put("B11	",	b11);
		a11.getSettoriAdiacenti().put("A12	",	a12);

		a12.getSettoriAdiacenti().put("A11	",	a11);
		a12.getSettoriAdiacenti().put("B11	",	b11);
		a12.getSettoriAdiacenti().put("B12	",	b12);
		a12.getSettoriAdiacenti().put("A13	",	a13);
		
		a13.getSettoriAdiacenti().put("A12	",	a12);
		a13.getSettoriAdiacenti().put("B12	",	b12);
		a13.getSettoriAdiacenti().put("Z04	",	scialuppa4);
		a13.getSettoriAdiacenti().put("A14	",	a14);
		
		a14.getSettoriAdiacenti().put("A13	",	a13);
		a14.getSettoriAdiacenti().put("Z04	",	scialuppa4);
		a14.getSettoriAdiacenti().put("B14	",	b14);
		
		//adiacenti B		
		b1.getSettoriAdiacenti().put("A02",a2);
		b1.getSettoriAdiacenti().put("Z01",scialuppa1);
		b1.getSettoriAdiacenti().put("C02",c2);
		b1.getSettoriAdiacenti().put("C01",c1);
	
		b3.getSettoriAdiacenti().put("A03",a3);
		b3.getSettoriAdiacenti().put("A04",a4);
		b3.getSettoriAdiacenti().put("Z01",scialuppa1);
		b3.getSettoriAdiacenti().put("B04",b4);
		b3.getSettoriAdiacenti().put("C03",c3);
		b3.getSettoriAdiacenti().put("C04",c4);
		
		b4.getSettoriAdiacenti().put("A04",a4);
		b4.getSettoriAdiacenti().put("A05",a5);
		b4.getSettoriAdiacenti().put("B03",b3);
		b4.getSettoriAdiacenti().put("B05",b5);
		b4.getSettoriAdiacenti().put("C04",c4);
		b4.getSettoriAdiacenti().put("C05",c5);
		b5.getSettoriAdiacenti().put("A05",a5);
		b5.getSettoriAdiacenti().put("A06",a6);
		b5.getSettoriAdiacenti().put("B04",b4);
		b5.getSettoriAdiacenti().put("B06",b6);
		b5.getSettoriAdiacenti().put("C05",c5);
		b5.getSettoriAdiacenti().put("C06",c6);
		b6.getSettoriAdiacenti().put("A06",a6);
		b6.getSettoriAdiacenti().put("B05",b5);
		b6.getSettoriAdiacenti().put("C06",c6);
		b6.getSettoriAdiacenti().put("C07",c7);

		b8.getSettoriAdiacenti().put("A09",a9);
		b8.getSettoriAdiacenti().put("B09",b9);
		b8.getSettoriAdiacenti().put("C08",c8);
		b8.getSettoriAdiacenti().put("C09",c9);
		
		b9.getSettoriAdiacenti().put("A09",a9	);
		b9.getSettoriAdiacenti().put("A10",	a10	);
		b9.getSettoriAdiacenti().put("B08",	b8	);
		b9.getSettoriAdiacenti().put("B10",	b10	);
		b9.getSettoriAdiacenti().put("C09",	c9	);
		b9.getSettoriAdiacenti().put("C10",	c10	);
		
		b10.getSettoriAdiacenti().put("A10",a10	);
		b10.getSettoriAdiacenti().put("A11",a11	);
		b10.getSettoriAdiacenti().put("B09",b9	);
		b10.getSettoriAdiacenti().put("B11",b11	);
		b10.getSettoriAdiacenti().put("C10",c10	);
		b10.getSettoriAdiacenti().put("C11",c11	);
		
		b11.getSettoriAdiacenti().put("A11",a11	);
		b11.getSettoriAdiacenti().put("A12",a12	);
		b11.getSettoriAdiacenti().put("B10",b10	);
		b11.getSettoriAdiacenti().put("B12",b12	);
		b11.getSettoriAdiacenti().put("C11",c11	);
		b11.getSettoriAdiacenti().put("C12",c12	);
		
		b12.getSettoriAdiacenti().put("A12",a12	);
		b12.getSettoriAdiacenti().put("A13",a13	);
		b12.getSettoriAdiacenti().put("B11",b11	);
		b12.getSettoriAdiacenti().put("Z04",scialuppa4	);
		b12.getSettoriAdiacenti().put("C12",c12	);
		b12.getSettoriAdiacenti().put("C13",c13	);
		
		b14.getSettoriAdiacenti().put("A14",a14	);
		b14.getSettoriAdiacenti().put("Z04",scialuppa4	);
		b14.getSettoriAdiacenti().put("C14",c14	);
		
		//adiacenti C		
		c1.getSettoriAdiacenti().put("B01",b1);
		c1.getSettoriAdiacenti().put("C02",c2);
		
		c2.getSettoriAdiacenti().put("C01",c1);
		c2.getSettoriAdiacenti().put("B01",b1);
		c2.getSettoriAdiacenti().put("Z04",scialuppa4);
		c2.getSettoriAdiacenti().put("C03",c3);
		c2.getSettoriAdiacenti().put("D02",d2);
		
		c3.getSettoriAdiacenti().put("C02",c2);
		c3.getSettoriAdiacenti().put("Z04",scialuppa4);
		c3.getSettoriAdiacenti().put("B03",b3);
		c3.getSettoriAdiacenti().put("C04",c4);
		c3.getSettoriAdiacenti().put("D02",d2);
		c3.getSettoriAdiacenti().put("D03",d3);
		
		c4.getSettoriAdiacenti().put("B03",b3);
		c4.getSettoriAdiacenti().put("B04",b4);
		c4.getSettoriAdiacenti().put("C03",c3);
		c4.getSettoriAdiacenti().put("C05",c5);
		c4.getSettoriAdiacenti().put("D03",d3);
		
		c5.getSettoriAdiacenti().put("B04",b4);
		c5.getSettoriAdiacenti().put("B05",b5);
		c5.getSettoriAdiacenti().put("C04",c4);
		c5.getSettoriAdiacenti().put("C06",c6);
		c5.getSettoriAdiacenti().put("D05",d5);
		
		c6.getSettoriAdiacenti().put("B05",b5);
		c6.getSettoriAdiacenti().put("B06",b6);
		c6.getSettoriAdiacenti().put("C05",c5);
		c6.getSettoriAdiacenti().put("C07",c7);
		c6.getSettoriAdiacenti().put("D05",d5);
		
		c7.getSettoriAdiacenti().put("B06",b6);
		c7.getSettoriAdiacenti().put("C06",c6);
		c7.getSettoriAdiacenti().put("C08",c8);
				
		c8.getSettoriAdiacenti().put("B08",b8);
		c8.getSettoriAdiacenti().put("C07",c7);
		c8.getSettoriAdiacenti().put("C09",c9);
		c8.getSettoriAdiacenti().put("D08",d8);
				
		c9.getSettoriAdiacenti().put("B08",b8);
		c9.getSettoriAdiacenti().put("B09",b9);
		c9.getSettoriAdiacenti().put("C08",c8);
		c9.getSettoriAdiacenti().put("C10",c10);
		c9.getSettoriAdiacenti().put("D08",d8);
		c9.getSettoriAdiacenti().put("D09",d9);
		
		c10.getSettoriAdiacenti().put("B09",b9);
		c10.getSettoriAdiacenti().put("B10",b10);
		c10.getSettoriAdiacenti().put("C09",c9);
		c10.getSettoriAdiacenti().put("C11",c11);
		c10.getSettoriAdiacenti().put("D09",d9);
		c10.getSettoriAdiacenti().put("D10",d10);
		
		c11.getSettoriAdiacenti().put("B10",b10);
		c11.getSettoriAdiacenti().put("B11",b11);
		c11.getSettoriAdiacenti().put("C10",c10);
		c11.getSettoriAdiacenti().put("C12",c12);
		c11.getSettoriAdiacenti().put("D10",d10);
		c11.getSettoriAdiacenti().put("D11",d11);
		
		c12.getSettoriAdiacenti().put("B11",b11);
		c12.getSettoriAdiacenti().put("B12",b12);
		c12.getSettoriAdiacenti().put("C11",c11);
		c12.getSettoriAdiacenti().put("C13",c13);
		c12.getSettoriAdiacenti().put("D11",d11);
		c12.getSettoriAdiacenti().put("D12",d12);
		
		c13.getSettoriAdiacenti().put("B12",b12);
		c13.getSettoriAdiacenti().put("Z04",scialuppa4);
		c13.getSettoriAdiacenti().put("C12",c12);
		c13.getSettoriAdiacenti().put("C14",c14);
		c13.getSettoriAdiacenti().put("D12",d12);
		c13.getSettoriAdiacenti().put("D13",d13);
		
		c14.getSettoriAdiacenti().put("Z04",scialuppa4);
		c14.getSettoriAdiacenti().put("B14",b14);
		c14.getSettoriAdiacenti().put("C13",c13);
		c14.getSettoriAdiacenti().put("D13",d13);
		c14.getSettoriAdiacenti().put("D14",d14);
		
		//adiacenti D
		d2.getSettoriAdiacenti().put("C02",c2);
		d2.getSettoriAdiacenti().put("C03",c3);
		d2.getSettoriAdiacenti().put("D03",d3);
		d2.getSettoriAdiacenti().put("E02",e2);
		d2.getSettoriAdiacenti().put("E03",e3);
		
		d3.getSettoriAdiacenti().put("C03",c3);
		d3.getSettoriAdiacenti().put("C04",c4);
		d3.getSettoriAdiacenti().put("D02",d2);
		d3.getSettoriAdiacenti().put("E03",e3);
		d3.getSettoriAdiacenti().put("E04",e4);
		
		d5.getSettoriAdiacenti().put("C05",c5);
		d5.getSettoriAdiacenti().put("C06",c6);
		d5.getSettoriAdiacenti().put("E05",e5);
		d5.getSettoriAdiacenti().put("E06",e6);
		
		d8.getSettoriAdiacenti().put("C08",c8);
		d8.getSettoriAdiacenti().put("C09",c9);
		d8.getSettoriAdiacenti().put("D09",d9);
		d8.getSettoriAdiacenti().put("E08",e8);
		d8.getSettoriAdiacenti().put("E09",e9);
		
		d9.getSettoriAdiacenti().put("C09",c9);
		d9.getSettoriAdiacenti().put("C10",c10);
		d9.getSettoriAdiacenti().put("D08",d8);
		d9.getSettoriAdiacenti().put("D10",d10);
		d9.getSettoriAdiacenti().put("E09",e9);
		d9.getSettoriAdiacenti().put("E10",e10);
		
		d10.getSettoriAdiacenti().put("C10",c10);
		d10.getSettoriAdiacenti().put("C11",c11);
		d10.getSettoriAdiacenti().put("D09",d9);
		d10.getSettoriAdiacenti().put("D11",d11);
		d10.getSettoriAdiacenti().put("E10",e10);
		d10.getSettoriAdiacenti().put("E11",e11);
		
		d11.getSettoriAdiacenti().put("C11",c11);
		d11.getSettoriAdiacenti().put("C12",c12);
		d11.getSettoriAdiacenti().put("D10",d10);
		d11.getSettoriAdiacenti().put("D12",d12);
		d11.getSettoriAdiacenti().put("E11",e11);
		d11.getSettoriAdiacenti().put("E12",e12);
		
		d12.getSettoriAdiacenti().put("C12",c12);
		d12.getSettoriAdiacenti().put("C13",c13);
		d12.getSettoriAdiacenti().put("D11",d11);
		d12.getSettoriAdiacenti().put("D13",d13);
		d12.getSettoriAdiacenti().put("E12",e12);
		d12.getSettoriAdiacenti().put("E13",e13);
		
		d13.getSettoriAdiacenti().put("C13",c13);
		d13.getSettoriAdiacenti().put("C14",c14);
		d13.getSettoriAdiacenti().put("D12",d12);
		d13.getSettoriAdiacenti().put("D14",d14);
		d13.getSettoriAdiacenti().put("E13",e13);
		
		d14.getSettoriAdiacenti().put("C14",c14);
		d14.getSettoriAdiacenti().put("D13",d13);
		
		//adiacenti E
		e2.getSettoriAdiacenti().put("D02",d2);
		e2.getSettoriAdiacenti().put("E03",e3);
		e2.getSettoriAdiacenti().put("F01",f1);
		e2.getSettoriAdiacenti().put("F02",f2);
		
		e3.getSettoriAdiacenti().put("D02",d2);
		e3.getSettoriAdiacenti().put("D03",d3);
		e3.getSettoriAdiacenti().put("E02",e2);
		e3.getSettoriAdiacenti().put("E04",e4);
		e3.getSettoriAdiacenti().put("F02",f2);
		e3.getSettoriAdiacenti().put("F03",f3);
		
		e4.getSettoriAdiacenti().put("D03",d3);
		e4.getSettoriAdiacenti().put("E03",e3);
		e4.getSettoriAdiacenti().put("E05",e5);
		e4.getSettoriAdiacenti().put("F03",f3);
		e4.getSettoriAdiacenti().put("F04",f4);
		
		e5.getSettoriAdiacenti().put("D05",d5);
		e5.getSettoriAdiacenti().put("E04",e4);
		e5.getSettoriAdiacenti().put("E06",e6);
		e5.getSettoriAdiacenti().put("F04",f4);
		e5.getSettoriAdiacenti().put("F05",f5);
		
		e6.getSettoriAdiacenti().put("D05",d5);
		e6.getSettoriAdiacenti().put("E05",e5);
		e6.getSettoriAdiacenti().put("F05",f5);
		e6.getSettoriAdiacenti().put("F06",f6);
		
		e8.getSettoriAdiacenti().put("D08",d8);
		e8.getSettoriAdiacenti().put("E09",e9);
		e8.getSettoriAdiacenti().put("F07",f7);
		e8.getSettoriAdiacenti().put("F08",f8);
		
		e9.getSettoriAdiacenti().put("D08",d8);
		e9.getSettoriAdiacenti().put("D09",d9);
		e9.getSettoriAdiacenti().put("E08",e8);
		e9.getSettoriAdiacenti().put("E10",e10);
		e9.getSettoriAdiacenti().put("F08",f8);
		e9.getSettoriAdiacenti().put("F09",f9);
		
		e10.getSettoriAdiacenti().put("D09",d9);
		e10.getSettoriAdiacenti().put("D10",d10);
		e10.getSettoriAdiacenti().put("E09",e9);
		e10.getSettoriAdiacenti().put("E11",e11);
		e10.getSettoriAdiacenti().put("F09",f9);
		e10.getSettoriAdiacenti().put("F10",f10);
		
		e11.getSettoriAdiacenti().put("D10",d10);
		e11.getSettoriAdiacenti().put("D11",d11);
		e11.getSettoriAdiacenti().put("E10",e10);
		e11.getSettoriAdiacenti().put("E12",e12);
		e11.getSettoriAdiacenti().put("F10",f10);
		e11.getSettoriAdiacenti().put("F11",f11);
		
		e12.getSettoriAdiacenti().put("D11",d11);
		e12.getSettoriAdiacenti().put("D12",d12);
		e12.getSettoriAdiacenti().put("E11",e11);
		e12.getSettoriAdiacenti().put("E13",e13);
		e12.getSettoriAdiacenti().put("F11",f11);

		e13.getSettoriAdiacenti().put("D12",d12);
		e13.getSettoriAdiacenti().put("D13",d13);
		e13.getSettoriAdiacenti().put("E12",e12);

		//adiacenti F		
		f1.getSettoriAdiacenti().put("E02",e2);
		f1.getSettoriAdiacenti().put("F02",f2);
		f1.getSettoriAdiacenti().put("G01",g1);
		f1.getSettoriAdiacenti().put("G02",g2);
		
		f2.getSettoriAdiacenti().put("E02",e2);
		f2.getSettoriAdiacenti().put("E03",e3);
		f2.getSettoriAdiacenti().put("F01",f1);
		f2.getSettoriAdiacenti().put("F03",f3);
		f2.getSettoriAdiacenti().put("G02",g2);
		f2.getSettoriAdiacenti().put("G03",g3);
		
		f3.getSettoriAdiacenti().put("E03",e3);
		f3.getSettoriAdiacenti().put("E04",e4);
		f3.getSettoriAdiacenti().put("F02",f2);
		f3.getSettoriAdiacenti().put("F04",f4);
		f3.getSettoriAdiacenti().put("G03",g3);
		f3.getSettoriAdiacenti().put("G04",g4);
		
		f4.getSettoriAdiacenti().put("E04",e4);
		f4.getSettoriAdiacenti().put("E05",e5);
		f4.getSettoriAdiacenti().put("F03",f3);
		f4.getSettoriAdiacenti().put("F05",f5);
		f4.getSettoriAdiacenti().put("G04",g4);
		f4.getSettoriAdiacenti().put("G05",g5);
		
		f5.getSettoriAdiacenti().put("E05",e5);
		f5.getSettoriAdiacenti().put("E06",e6);
		f5.getSettoriAdiacenti().put("F04",f4);
		f5.getSettoriAdiacenti().put("F06",f6);
		f5.getSettoriAdiacenti().put("G05",g5);
		f5.getSettoriAdiacenti().put("G06",g6);
		
		f6.getSettoriAdiacenti().put("E06",e6);
		f6.getSettoriAdiacenti().put("F05",f5);
		f6.getSettoriAdiacenti().put("F07",f7);
		f6.getSettoriAdiacenti().put("G06",g6);
		f6.getSettoriAdiacenti().put("G07",g7);
		
		f7.getSettoriAdiacenti().put("E08",e8);
		f7.getSettoriAdiacenti().put("F06",f6);
		f7.getSettoriAdiacenti().put("F08",f8);
		f7.getSettoriAdiacenti().put("G07",g7);
		f7.getSettoriAdiacenti().put("G08",g8);
		
		f8.getSettoriAdiacenti().put("E08",e8);
		f8.getSettoriAdiacenti().put("E09",e9);
		f8.getSettoriAdiacenti().put("F07",f7);
		f8.getSettoriAdiacenti().put("F09",f9);
		f8.getSettoriAdiacenti().put("G08",g8);
		f8.getSettoriAdiacenti().put("G09",g9);
		
		f9.getSettoriAdiacenti().put("E09",e9);
		f9.getSettoriAdiacenti().put("E10",e10);
		f9.getSettoriAdiacenti().put("F08",f8);
		f9.getSettoriAdiacenti().put("F10",f10);
		f9.getSettoriAdiacenti().put("G09",g9);
		f9.getSettoriAdiacenti().put("G10",g10);
		
		f10.getSettoriAdiacenti().put("E10",e10);
		f10.getSettoriAdiacenti().put("E11",e11);
		f10.getSettoriAdiacenti().put("F09",f9);
		f10.getSettoriAdiacenti().put("F11",f11);
		f10.getSettoriAdiacenti().put("G10",g10);
		f10.getSettoriAdiacenti().put("G11",g11);
		
		f11.getSettoriAdiacenti().put("E11",e11);
		f11.getSettoriAdiacenti().put("E12",e12);
		f11.getSettoriAdiacenti().put("F10",f10);
		f11.getSettoriAdiacenti().put("G11",g11);
		f11.getSettoriAdiacenti().put("G12",g12);
		
		//adiacenti G
		g1.getSettoriAdiacenti().put("F01",f1);
		g1.getSettoriAdiacenti().put("G02",g2);
		g1.getSettoriAdiacenti().put("H01",h1);
		
		g2.getSettoriAdiacenti().put("F01",f1);
		g2.getSettoriAdiacenti().put("F02",f2);
		g2.getSettoriAdiacenti().put("G01",g1);
		g2.getSettoriAdiacenti().put("G03",g3);
		g2.getSettoriAdiacenti().put("H01",h1);
		g2.getSettoriAdiacenti().put("H02",h2);
		
		g3.getSettoriAdiacenti().put("F02",f2);
		g3.getSettoriAdiacenti().put("F03",f3);
		g3.getSettoriAdiacenti().put("G02",g2);
		g3.getSettoriAdiacenti().put("G04",g4);
		g3.getSettoriAdiacenti().put("H02",h2);
		g3.getSettoriAdiacenti().put("H03",h3);
		
		g4.getSettoriAdiacenti().put("F03",f3);
		g4.getSettoriAdiacenti().put("F04",f4);
		g4.getSettoriAdiacenti().put("G03",g3);
		g4.getSettoriAdiacenti().put("G05",g5);
		g4.getSettoriAdiacenti().put("H03",h3);
		g4.getSettoriAdiacenti().put("H04",h4);
		
		g5.getSettoriAdiacenti().put("F04",f4);
		g5.getSettoriAdiacenti().put("F05",f5);
		g5.getSettoriAdiacenti().put("G04",g4);
		g5.getSettoriAdiacenti().put("G06",g6);
		g5.getSettoriAdiacenti().put("H04",h4);

		g6.getSettoriAdiacenti().put("F05",f5);
		g6.getSettoriAdiacenti().put("F06",f6);
		g6.getSettoriAdiacenti().put("G05",g5);
		g6.getSettoriAdiacenti().put("G07",g7);
		g6.getSettoriAdiacenti().put("H06",h6);
		
		g7.getSettoriAdiacenti().put("F06",f6);
		g7.getSettoriAdiacenti().put("F07",f7);
		g7.getSettoriAdiacenti().put("G06",g6);
		g7.getSettoriAdiacenti().put("G08",g8);
		g7.getSettoriAdiacenti().put("H06",h6);
		g7.getSettoriAdiacenti().put("H07",h7);
		
		g8.getSettoriAdiacenti().put("F07",f7);
		g8.getSettoriAdiacenti().put("F08",f8);
		g8.getSettoriAdiacenti().put("G07",g7);
		g8.getSettoriAdiacenti().put("G09",g9);
		g8.getSettoriAdiacenti().put("H07",h7);
		g8.getSettoriAdiacenti().put("H08",h8);
		
		g9.getSettoriAdiacenti().put("F08",f8);
		g9.getSettoriAdiacenti().put("F09",f9);
		g9.getSettoriAdiacenti().put("G08",g8);
		g9.getSettoriAdiacenti().put("G10",g10);
		g9.getSettoriAdiacenti().put("H08",h8);
		g9.getSettoriAdiacenti().put("H09",h9);
		
		g10.getSettoriAdiacenti().put("F09",f9);
		g10.getSettoriAdiacenti().put("F10",f10);
		g10.getSettoriAdiacenti().put("G09",g9);
		g10.getSettoriAdiacenti().put("G11",g11);
		g10.getSettoriAdiacenti().put("H09",h9);

		g11.getSettoriAdiacenti().put("F10",f10);
		g11.getSettoriAdiacenti().put("F11",f11);
		g11.getSettoriAdiacenti().put("G10",g10);
		g11.getSettoriAdiacenti().put("G12",g12);

		g12.getSettoriAdiacenti().put("F11",f11);
		g12.getSettoriAdiacenti().put("G11",g11);
		g12.getSettoriAdiacenti().put("G13",g13);
		g12.getSettoriAdiacenti().put("H12",h12);
		
		g13.getSettoriAdiacenti().put("G12",g12);
		g13.getSettoriAdiacenti().put("G14",g14);
		g13.getSettoriAdiacenti().put("H12",h12);
		g13.getSettoriAdiacenti().put("H13",h13);

		g14.getSettoriAdiacenti().put("G13",g13);
		g14.getSettoriAdiacenti().put("H13",h13);
		g14.getSettoriAdiacenti().put("H14",h14);

		
		//adiacenti H
		h1.getSettoriAdiacenti().put("G01",g1);
		h1.getSettoriAdiacenti().put("G02",g2);
		h1.getSettoriAdiacenti().put("H02",h2);
		h1.getSettoriAdiacenti().put("I01",i1);
		h1.getSettoriAdiacenti().put("I02",i2);
		
		h2.getSettoriAdiacenti().put("G02",g2);
		h2.getSettoriAdiacenti().put("G03",g3);
		h2.getSettoriAdiacenti().put("H01",h1);
		h2.getSettoriAdiacenti().put("H03",h3);
		h2.getSettoriAdiacenti().put("I02",i2);
		h2.getSettoriAdiacenti().put("I03",i3);
		
		h3.getSettoriAdiacenti().put("G03",g3);
		h3.getSettoriAdiacenti().put("G04",g4);
		h3.getSettoriAdiacenti().put("H02",h2);
		h3.getSettoriAdiacenti().put("H04",h4);
		h3.getSettoriAdiacenti().put("I03",i3);
		h3.getSettoriAdiacenti().put("I04",i4);
		
		h4.getSettoriAdiacenti().put("G04",g4);
		h4.getSettoriAdiacenti().put("G05",g5);
		h4.getSettoriAdiacenti().put("H03",h3);
		h4.getSettoriAdiacenti().put("I04",i4);
		h4.getSettoriAdiacenti().put("I05",i5);
		
		h6.getSettoriAdiacenti().put("G06",g6);
		h6.getSettoriAdiacenti().put("G07",g7);
		h6.getSettoriAdiacenti().put("H07",h7);
		h6.getSettoriAdiacenti().put("I07",i7);
		
		h7.getSettoriAdiacenti().put("G07",g7);
		h7.getSettoriAdiacenti().put("G08",g8);
		h7.getSettoriAdiacenti().put("H06",h6);
		h7.getSettoriAdiacenti().put("H08",h8);
		h7.getSettoriAdiacenti().put("I07",i7);
		h7.getSettoriAdiacenti().put("I08",i8);
		
		h8.getSettoriAdiacenti().put("G08",g8);
		h8.getSettoriAdiacenti().put("G09",g9);
		h8.getSettoriAdiacenti().put("H07",h7);
		h8.getSettoriAdiacenti().put("H09",h9);
		h8.getSettoriAdiacenti().put("I08",i8);
		h8.getSettoriAdiacenti().put("I09",i9);
		
		h9.getSettoriAdiacenti().put("G09",g9);
		h9.getSettoriAdiacenti().put("G10",g10);
		h9.getSettoriAdiacenti().put("H08",h8);
		h9.getSettoriAdiacenti().put("I09",i9);
		h9.getSettoriAdiacenti().put("I10",i10);
		
		h12.getSettoriAdiacenti().put("G12",g12);
		h12.getSettoriAdiacenti().put("G13",g13);
		h12.getSettoriAdiacenti().put("H13",h13);
		h12.getSettoriAdiacenti().put("I13",i13);
		
		h13.getSettoriAdiacenti().put("G13",g13);
		h13.getSettoriAdiacenti().put("G14",g14);
		h13.getSettoriAdiacenti().put("H12",h12);
		h13.getSettoriAdiacenti().put("H14",h14);
		h13.getSettoriAdiacenti().put("I13",i13);
		h13.getSettoriAdiacenti().put("I14",i14);
		
		h14.getSettoriAdiacenti().put("G14",g14);
		h14.getSettoriAdiacenti().put("H13",h13);
		h14.getSettoriAdiacenti().put("I14",i14);
		
		//adiacenti I 
		i1.getSettoriAdiacenti().put("H01",h1);
		i1.getSettoriAdiacenti().put("I02",i2);
		i1.getSettoriAdiacenti().put("J01",j1);
		
		i2.getSettoriAdiacenti().put("H01",h1);
		i2.getSettoriAdiacenti().put("H02",h2);
		i2.getSettoriAdiacenti().put("I01",i1);
		i2.getSettoriAdiacenti().put("I03",i3);
		i2.getSettoriAdiacenti().put("J01",j1);
		i2.getSettoriAdiacenti().put("J02",j2);
		
		i3.getSettoriAdiacenti().put("H02",h2);
		i3.getSettoriAdiacenti().put("H03",h3);
		i3.getSettoriAdiacenti().put("I02",i2);
		i3.getSettoriAdiacenti().put("I04",i4);
		i3.getSettoriAdiacenti().put("J02",j2);
		i3.getSettoriAdiacenti().put("J03",j3);
		
		i4.getSettoriAdiacenti().put("H03",h3);
		i4.getSettoriAdiacenti().put("H04",h4);
		i4.getSettoriAdiacenti().put("I03",i3);
		i4.getSettoriAdiacenti().put("I05",i5);
		i4.getSettoriAdiacenti().put("J03",j3);
		i4.getSettoriAdiacenti().put("J04",j4);
		
		i5.getSettoriAdiacenti().put("H04",h4);
		i5.getSettoriAdiacenti().put("I04",i4);
		i5.getSettoriAdiacenti().put("J04",j4);
		i5.getSettoriAdiacenti().put("J05",j5);
		
		i7.getSettoriAdiacenti().put("H06",h6);
		i7.getSettoriAdiacenti().put("H07",h7);
		i7.getSettoriAdiacenti().put("I08",i8);
		i7.getSettoriAdiacenti().put("J06",j6);

		i8.getSettoriAdiacenti().put("H07",h7);
		i8.getSettoriAdiacenti().put("H08",h8);
		i8.getSettoriAdiacenti().put("I07",i7);
		i8.getSettoriAdiacenti().put("I09",i9);
		i8.getSettoriAdiacenti().put("J08",j8);
		
		i9.getSettoriAdiacenti().put("H08",h8);
		i9.getSettoriAdiacenti().put("H09",h9);
		i9.getSettoriAdiacenti().put("I08",i8);
		i9.getSettoriAdiacenti().put("I10",i10);
		i9.getSettoriAdiacenti().put("J08",j8);
		i9.getSettoriAdiacenti().put("J09",j9);
		
		i10.getSettoriAdiacenti().put("H09",h9);
		i10.getSettoriAdiacenti().put("I09",i9);
		i10.getSettoriAdiacenti().put("I11",i11);
		i10.getSettoriAdiacenti().put("J09",j9);
		i10.getSettoriAdiacenti().put("J10",j10);
		
		i11.getSettoriAdiacenti().put("I10",i10);
		i11.getSettoriAdiacenti().put("J10",j10);
		i11.getSettoriAdiacenti().put("J11",j11);

		i13.getSettoriAdiacenti().put("H12",h12);
		i13.getSettoriAdiacenti().put("H13",h13);
		i13.getSettoriAdiacenti().put("I14",i14);
		i13.getSettoriAdiacenti().put("J13",j13);
		
		i14.getSettoriAdiacenti().put("H13",h13);
		i14.getSettoriAdiacenti().put("H14",h14);
		i14.getSettoriAdiacenti().put("I13",i13);
		i14.getSettoriAdiacenti().put("J13",j13);
		i14.getSettoriAdiacenti().put("J14",j14);

		//adiacenti J
		j1.getSettoriAdiacenti().put("I01",i1);
		j1.getSettoriAdiacenti().put("I02",i2);
		j1.getSettoriAdiacenti().put("J02",j2);
		j1.getSettoriAdiacenti().put("K01",k1);
		j1.getSettoriAdiacenti().put("K02",k2);
		
		j2.getSettoriAdiacenti().put("I02",i2);
		j2.getSettoriAdiacenti().put("I03",i3);
		j2.getSettoriAdiacenti().put("J01",j1);
		j2.getSettoriAdiacenti().put("J03",j3);
		j2.getSettoriAdiacenti().put("K02",k2);
		j2.getSettoriAdiacenti().put("K03",k3);
		
		j3.getSettoriAdiacenti().put("I03",i3);
		j3.getSettoriAdiacenti().put("I04",i4);
		j3.getSettoriAdiacenti().put("J02",j2);
		j3.getSettoriAdiacenti().put("J04",j4);
		j3.getSettoriAdiacenti().put("K03",k3);
		j3.getSettoriAdiacenti().put("K04",k4);
		
		j4.getSettoriAdiacenti().put("I04",i4);
		j4.getSettoriAdiacenti().put("I05",i5);
		j4.getSettoriAdiacenti().put("J03",j3);
		j4.getSettoriAdiacenti().put("J05",j5);
		j4.getSettoriAdiacenti().put("K04",k4);
		j4.getSettoriAdiacenti().put("K05",k5);
		
		j5.getSettoriAdiacenti().put("I05",i5);
		j5.getSettoriAdiacenti().put("J04",j4);
		j5.getSettoriAdiacenti().put("J06",j6);
		j5.getSettoriAdiacenti().put("K05",k5);
		j5.getSettoriAdiacenti().put("K06",k6);
		
		j6.getSettoriAdiacenti().put("I07",i7);
		j6.getSettoriAdiacenti().put("J05",j5);
		j6.getSettoriAdiacenti().put("K06",k6);
		
		j8.getSettoriAdiacenti().put("I08",i8);
		j8.getSettoriAdiacenti().put("I09",i9);
		j8.getSettoriAdiacenti().put("J09",j9);
		j8.getSettoriAdiacenti().put("K08",k8);
		j8.getSettoriAdiacenti().put("K09",k9);
		
		j9.getSettoriAdiacenti().put("I09",i9);
		j9.getSettoriAdiacenti().put("I10",i10);
		j9.getSettoriAdiacenti().put("J08",j8);
		j9.getSettoriAdiacenti().put("J10",j10);
		j9.getSettoriAdiacenti().put("K09",k9);
		j9.getSettoriAdiacenti().put("K10",k10);
		
		j10.getSettoriAdiacenti().put("I10",i10);
		j10.getSettoriAdiacenti().put("I11",i11);
		j10.getSettoriAdiacenti().put("J09",j9);
		j10.getSettoriAdiacenti().put("J11",j11);
		j10.getSettoriAdiacenti().put("K10",k10);
		j10.getSettoriAdiacenti().put("K11",k11);
		
		j11.getSettoriAdiacenti().put("I11",i11);
		j11.getSettoriAdiacenti().put("J10",j10);
		j11.getSettoriAdiacenti().put("K11",k11);
		j11.getSettoriAdiacenti().put("K12",k12);
		
		j13.getSettoriAdiacenti().put("I13",i13);
		j13.getSettoriAdiacenti().put("I14",i14);
		j13.getSettoriAdiacenti().put("J14",j14);
		j13.getSettoriAdiacenti().put("K14",k14);
		
		j14.getSettoriAdiacenti().put("I14",i14);
		j14.getSettoriAdiacenti().put("J13",j13);
		j14.getSettoriAdiacenti().put("K14",k14);
		
		//adiacenti K
		k1.getSettoriAdiacenti().put("J01",j1);
		k1.getSettoriAdiacenti().put("K02",k2);
		k1.getSettoriAdiacenti().put("L01",l1);
		
		k2.getSettoriAdiacenti().put("J01",j1);
		k2.getSettoriAdiacenti().put("J02",j2);
		k2.getSettoriAdiacenti().put("K01",k1);
		k2.getSettoriAdiacenti().put("K03",k3);
		k2.getSettoriAdiacenti().put("L01",l1);
		k2.getSettoriAdiacenti().put("L02",l2);
		
		k3.getSettoriAdiacenti().put("J02",j2);
		k3.getSettoriAdiacenti().put("J03",j3);
		k3.getSettoriAdiacenti().put("K02",k2);
		k3.getSettoriAdiacenti().put("K04",k4);
		k3.getSettoriAdiacenti().put("L02",l2);
		k3.getSettoriAdiacenti().put("L03",l3);
		
		k4.getSettoriAdiacenti().put("J03",j3);
		k4.getSettoriAdiacenti().put("J04",j4);
		k4.getSettoriAdiacenti().put("K03",k3);
		k4.getSettoriAdiacenti().put("K05",k5);
		k4.getSettoriAdiacenti().put("L03",l3);
		k4.getSettoriAdiacenti().put("L04",l4);
		
		k5.getSettoriAdiacenti().put("J04",j4);
		k5.getSettoriAdiacenti().put("J05",j5);
		k5.getSettoriAdiacenti().put("K04",k4);
		k5.getSettoriAdiacenti().put("K06",k6);
		k5.getSettoriAdiacenti().put("L04",l4);
		k5.getSettoriAdiacenti().put("L05",l5);
		
		k6.getSettoriAdiacenti().put("J05",j5);
		k6.getSettoriAdiacenti().put("J06",j6);
		k6.getSettoriAdiacenti().put("K05",k5);
		k6.getSettoriAdiacenti().put("L05",l5);
		k6.getSettoriAdiacenti().put("XA",baseAlieni);
		
		k8.getSettoriAdiacenti().put("J08",j8);
		k8.getSettoriAdiacenti().put("K09",k9);
		k8.getSettoriAdiacenti().put("XU",baseUmani);
		
		k9.getSettoriAdiacenti().put("J08",j8);
		k9.getSettoriAdiacenti().put("J09",j9);
		k9.getSettoriAdiacenti().put("K08",k8);
		k9.getSettoriAdiacenti().put("K10",k10);
		k9.getSettoriAdiacenti().put("XU",baseUmani);
		k9.getSettoriAdiacenti().put("L09",l9);
		
		k10.getSettoriAdiacenti().put("J09",j9);
		k10.getSettoriAdiacenti().put("J10",j10);
		k10.getSettoriAdiacenti().put("K09",k9);
		k10.getSettoriAdiacenti().put("K11",k11);
		k10.getSettoriAdiacenti().put("L09",l9);
		k10.getSettoriAdiacenti().put("L10",l10);
		
		k11.getSettoriAdiacenti().put("J10",j10);
		k11.getSettoriAdiacenti().put("J11",j11);
		k11.getSettoriAdiacenti().put("K10",k10);
		k11.getSettoriAdiacenti().put("K12",k12);
		k11.getSettoriAdiacenti().put("L10",l10);
		k11.getSettoriAdiacenti().put("L11",l11);
		
		k12.getSettoriAdiacenti().put("J11",j11);
		k12.getSettoriAdiacenti().put("K11",k11);
		k12.getSettoriAdiacenti().put("L11",l11);
		k12.getSettoriAdiacenti().put("L12",l12);

		k14.getSettoriAdiacenti().put("J13",j13);
		k14.getSettoriAdiacenti().put("J14",j14);
		k14.getSettoriAdiacenti().put("L13",l13);
		k14.getSettoriAdiacenti().put("L14",l14);

		
		//adiacenti L
		l1.getSettoriAdiacenti().put("K01",k1);
		l1.getSettoriAdiacenti().put("K02",k2);
		l1.getSettoriAdiacenti().put("L02",l2);
		l1.getSettoriAdiacenti().put("M01",m1);
		l1.getSettoriAdiacenti().put("M02",m2);
		
		l2.getSettoriAdiacenti().put("K02",k2);
		l2.getSettoriAdiacenti().put("K03",k3);
		l2.getSettoriAdiacenti().put("L01",l1);
		l2.getSettoriAdiacenti().put("L03",l3);
		l2.getSettoriAdiacenti().put("M02",m2);
		l2.getSettoriAdiacenti().put("M03",m3);
		
		l3.getSettoriAdiacenti().put("K03",k3);
		l3.getSettoriAdiacenti().put("K04",k4);
		l3.getSettoriAdiacenti().put("L02",l2);
		l3.getSettoriAdiacenti().put("L04",l4);
		l3.getSettoriAdiacenti().put("M03",m3);
		l3.getSettoriAdiacenti().put("M04",m4);
		
		l4.getSettoriAdiacenti().put("K04",k4);
		l4.getSettoriAdiacenti().put("K05",k5);
		l4.getSettoriAdiacenti().put("L03",l3);
		l4.getSettoriAdiacenti().put("L05",l5);
		l4.getSettoriAdiacenti().put("M04",m4);
		l4.getSettoriAdiacenti().put("M05",m5);
		
		l5.getSettoriAdiacenti().put("K05",k5);
		l5.getSettoriAdiacenti().put("K06",k6);
		l5.getSettoriAdiacenti().put("L04",l4);
		l5.getSettoriAdiacenti().put("XA", baseAlieni);
		l5.getSettoriAdiacenti().put("M05",m5);
		l5.getSettoriAdiacenti().put("M06",m6);
		
		l9.getSettoriAdiacenti().put("K09",k9);
		l9.getSettoriAdiacenti().put("K10",k10);
		l9.getSettoriAdiacenti().put("XU",baseUmani);
		l9.getSettoriAdiacenti().put("L10",l10);
		l9.getSettoriAdiacenti().put("M09",m9);
		l9.getSettoriAdiacenti().put("M10",m10);
		
		l10.getSettoriAdiacenti().put("K10",k10);
		l10.getSettoriAdiacenti().put("K11",k11);
		l10.getSettoriAdiacenti().put("L09",l9);
		l10.getSettoriAdiacenti().put("L11",l11);
		l10.getSettoriAdiacenti().put("M10",m10);
		l10.getSettoriAdiacenti().put("M11",m11);
		
		l11.getSettoriAdiacenti().put("K11",k11);
		l11.getSettoriAdiacenti().put("K12",k12);
		l11.getSettoriAdiacenti().put("L10",l10);
		l11.getSettoriAdiacenti().put("L12",l12);
		l11.getSettoriAdiacenti().put("M11",m11);
		l11.getSettoriAdiacenti().put("M12",m12);
		
		l12.getSettoriAdiacenti().put("K12",k12);
		l12.getSettoriAdiacenti().put("L11",l11);
		l12.getSettoriAdiacenti().put("L13",l13);
		l12.getSettoriAdiacenti().put("M12",m12);
		l12.getSettoriAdiacenti().put("M13",m13);
		
		l13.getSettoriAdiacenti().put("K14",k14);
		l13.getSettoriAdiacenti().put("L12",l12);
		l13.getSettoriAdiacenti().put("L14",l14);
		l13.getSettoriAdiacenti().put("M13",m13);
		l13.getSettoriAdiacenti().put("M14",m14);
		
		l14.getSettoriAdiacenti().put("K14",k14);
		l14.getSettoriAdiacenti().put("L13",l13);
		l14.getSettoriAdiacenti().put("M14",m14);
		
		//adiacenti M
		m1.getSettoriAdiacenti().put("L01",l1);
		m1.getSettoriAdiacenti().put("M02",m2);
		m1.getSettoriAdiacenti().put("N01",n1);
		
		m2.getSettoriAdiacenti().put("L01",l1);
		m2.getSettoriAdiacenti().put("L02",l2);
		m2.getSettoriAdiacenti().put("M01",m1);
		m2.getSettoriAdiacenti().put("M03",m3);
		m2.getSettoriAdiacenti().put("N01",n1);
		m2.getSettoriAdiacenti().put("N02",n2);
		
		m3.getSettoriAdiacenti().put("L02",l2);
		m3.getSettoriAdiacenti().put("L03",l3);
		m3.getSettoriAdiacenti().put("M02",m2);
		m3.getSettoriAdiacenti().put("M04",m4);
		m3.getSettoriAdiacenti().put("N02",n2);
		m3.getSettoriAdiacenti().put("N03",n3);
		
		m4.getSettoriAdiacenti().put("L03",l3);
		m4.getSettoriAdiacenti().put("L04",l4);
		m4.getSettoriAdiacenti().put("M03",m3);
		m4.getSettoriAdiacenti().put("M05",m5);
		m4.getSettoriAdiacenti().put("N03",n3);
		m4.getSettoriAdiacenti().put("N04",n4);
		
		m5.getSettoriAdiacenti().put("L04",l4);
		m5.getSettoriAdiacenti().put("L05",l5);
		m5.getSettoriAdiacenti().put("M04",m4);
		m5.getSettoriAdiacenti().put("M06",m6);
		m5.getSettoriAdiacenti().put("N04",n4);
		m5.getSettoriAdiacenti().put("N05",n5);
		
		m6.getSettoriAdiacenti().put("L05",l5);
		m6.getSettoriAdiacenti().put("XA",baseAlieni);
		m6.getSettoriAdiacenti().put("M05",m5);
		m6.getSettoriAdiacenti().put("N05",n5);
		m6.getSettoriAdiacenti().put("N06",n6);

		m8.getSettoriAdiacenti().put("XU",baseUmani);
		m8.getSettoriAdiacenti().put("M09",m9);
		m8.getSettoriAdiacenti().put("N08",n8);

		m9.getSettoriAdiacenti().put("XU",baseUmani);
		m9.getSettoriAdiacenti().put("L09",l9);
		m9.getSettoriAdiacenti().put("M08",m8);
		m9.getSettoriAdiacenti().put("M10",m10);
		m9.getSettoriAdiacenti().put("N08",n8);
		m9.getSettoriAdiacenti().put("N09",n9);
		
		m10.getSettoriAdiacenti().put("L09",l9);
		m10.getSettoriAdiacenti().put("L10",l10);
		m10.getSettoriAdiacenti().put("M09",m9);
		m10.getSettoriAdiacenti().put("M11",m11);
		m10.getSettoriAdiacenti().put("N09",n9);
		m10.getSettoriAdiacenti().put("N10",n10);
		
		m11.getSettoriAdiacenti().put("L10",l10);
		m11.getSettoriAdiacenti().put("L11",l11);
		m11.getSettoriAdiacenti().put("M10",m10);
		m11.getSettoriAdiacenti().put("M12",m12);
		m11.getSettoriAdiacenti().put("N10",n10);
		m11.getSettoriAdiacenti().put("N11",n11);
		
		m12.getSettoriAdiacenti().put("L11",l11);
		m12.getSettoriAdiacenti().put("L12",l12);
		m12.getSettoriAdiacenti().put("M11",m11);
		m12.getSettoriAdiacenti().put("M13",m13);
		m12.getSettoriAdiacenti().put("N11",n11);
		m12.getSettoriAdiacenti().put("N12",n12);
		
		m13.getSettoriAdiacenti().put("L12",l12);
		m13.getSettoriAdiacenti().put("L13",l13);
		m13.getSettoriAdiacenti().put("M12",m12);
		m13.getSettoriAdiacenti().put("M14",m14);
		m13.getSettoriAdiacenti().put("N12",n12);
		m13.getSettoriAdiacenti().put("N13",n13);
		
		m14.getSettoriAdiacenti().put("L13",l13);
		m14.getSettoriAdiacenti().put("L14",l14);
		m14.getSettoriAdiacenti().put("M13",m13);
		m14.getSettoriAdiacenti().put("N13",n13);
		m14.getSettoriAdiacenti().put("N14",n14);

		
		//adiacenti N
		n1.getSettoriAdiacenti().put("M01",m1);
		n1.getSettoriAdiacenti().put("M02",m2);
		n1.getSettoriAdiacenti().put("N02",n2);
		n1.getSettoriAdiacenti().put("O02",o2);
		
		n2.getSettoriAdiacenti().put("M02",m2);
		n2.getSettoriAdiacenti().put("M03",m3);
		n2.getSettoriAdiacenti().put("N01",n1);
		n2.getSettoriAdiacenti().put("N03",n3);
		n2.getSettoriAdiacenti().put("O02",o2);
		
		n3.getSettoriAdiacenti().put("M03",m3);
		n3.getSettoriAdiacenti().put("M04",m4);
		n3.getSettoriAdiacenti().put("N02",n2);
		n3.getSettoriAdiacenti().put("N04",n4);

		n4.getSettoriAdiacenti().put("M04",m4);
		n4.getSettoriAdiacenti().put("M05",m5);
		n4.getSettoriAdiacenti().put("N03",n3);
		n4.getSettoriAdiacenti().put("N05",n5);
		n4.getSettoriAdiacenti().put("O05",o5);
		
		n5.getSettoriAdiacenti().put("M05",m5);
		n5.getSettoriAdiacenti().put("M06",m6);
		n5.getSettoriAdiacenti().put("N04",n4);
		n5.getSettoriAdiacenti().put("N06",n6);
		n5.getSettoriAdiacenti().put("O05",o5);
		n5.getSettoriAdiacenti().put("O06",o6);
		
		n6.getSettoriAdiacenti().put("M06",m6);
		n6.getSettoriAdiacenti().put("N05",n5);
		n6.getSettoriAdiacenti().put("O06",o6);
		n6.getSettoriAdiacenti().put("O07",o7);

		n8.getSettoriAdiacenti().put("M08",m8);
		n8.getSettoriAdiacenti().put("M09",m9);
		n8.getSettoriAdiacenti().put("N09",n9);
		n8.getSettoriAdiacenti().put("O08",o8);
		n8.getSettoriAdiacenti().put("O09",o9);
		
		n9.getSettoriAdiacenti().put("M09",m9);
		n9.getSettoriAdiacenti().put("M10",m10);
		n9.getSettoriAdiacenti().put("N08",n8);
		n9.getSettoriAdiacenti().put("N10",n10);
		n9.getSettoriAdiacenti().put("O09",o9);
		n9.getSettoriAdiacenti().put("O10",o10);
		
		n10.getSettoriAdiacenti().put("M10",m10);
		n10.getSettoriAdiacenti().put("M11",m11);
		n10.getSettoriAdiacenti().put("N09",n9);
		n10.getSettoriAdiacenti().put("N11",n11);
		n10.getSettoriAdiacenti().put("O10",o10);
		n10.getSettoriAdiacenti().put("O11",o11);
		
		n11.getSettoriAdiacenti().put("M11",m11);
		n11.getSettoriAdiacenti().put("M12",m12);
		n11.getSettoriAdiacenti().put("N10",n10);
		n11.getSettoriAdiacenti().put("N12",n12);
		n11.getSettoriAdiacenti().put("O11",o11);
		n11.getSettoriAdiacenti().put("O12",o12);
		
		n12.getSettoriAdiacenti().put("M12",m12);
		n12.getSettoriAdiacenti().put("M13",m13);
		n12.getSettoriAdiacenti().put("N11",n11);
		n12.getSettoriAdiacenti().put("N13",n13);
		n12.getSettoriAdiacenti().put("O12",o12);
		n12.getSettoriAdiacenti().put("O13",o13);
		
		n13.getSettoriAdiacenti().put("M13",m13);
		n13.getSettoriAdiacenti().put("M14",m14);
		n13.getSettoriAdiacenti().put("N12",n12);
		n13.getSettoriAdiacenti().put("N14",n14);
		n13.getSettoriAdiacenti().put("O13",o13);
		n13.getSettoriAdiacenti().put("O14",o14);
		
		n14.getSettoriAdiacenti().put("M14",m14);
		n14.getSettoriAdiacenti().put("N13",n13);
		n14.getSettoriAdiacenti().put("O14",o14);
		
		//adiacenti O
		o2.getSettoriAdiacenti().put("N01",n1);
		o2.getSettoriAdiacenti().put("N02",n2);
		o2.getSettoriAdiacenti().put("P01",p1);
		o2.getSettoriAdiacenti().put("P02",p2);
		o5.getSettoriAdiacenti().put("N04",n4);
		o5.getSettoriAdiacenti().put("N05",n5);
		o5.getSettoriAdiacenti().put("O06",o6);
		o5.getSettoriAdiacenti().put("P04",p4);
		o5.getSettoriAdiacenti().put("P05",p5);
		
		o6.getSettoriAdiacenti().put("N05",n5);
		o6.getSettoriAdiacenti().put("N06",n6);
		o6.getSettoriAdiacenti().put("O05",o5);
		o6.getSettoriAdiacenti().put("O07",o7);
		o6.getSettoriAdiacenti().put("P05",p5);
		o6.getSettoriAdiacenti().put("P06",p6);
		
		o7.getSettoriAdiacenti().put("N06",n6);
		o7.getSettoriAdiacenti().put("O06",o6);
		o7.getSettoriAdiacenti().put("O08",o8);
		o7.getSettoriAdiacenti().put("P06",p6);
		o7.getSettoriAdiacenti().put("P07",p7);

		o8.getSettoriAdiacenti().put("N08",n8);
		o8.getSettoriAdiacenti().put("O07",o7);
		o8.getSettoriAdiacenti().put("O09",o9);
		o8.getSettoriAdiacenti().put("P07",p7);
		o8.getSettoriAdiacenti().put("P08",p8);
		
		o9.getSettoriAdiacenti().put("N08",n8);
		o9.getSettoriAdiacenti().put("N09",n9);
		o9.getSettoriAdiacenti().put("O08",o8);
		o9.getSettoriAdiacenti().put("O10",o10);
		o9.getSettoriAdiacenti().put("P08",p8);
		o9.getSettoriAdiacenti().put("P09",p9);
		
		o10.getSettoriAdiacenti().put("N09",n9);
		o10.getSettoriAdiacenti().put("N10",n10);
		o10.getSettoriAdiacenti().put("O09",o9);
		o10.getSettoriAdiacenti().put("O11",o11);
		o10.getSettoriAdiacenti().put("P09",p9);
		o10.getSettoriAdiacenti().put("P10",p10);
		
		o11.getSettoriAdiacenti().put("N10",n10);
		o11.getSettoriAdiacenti().put("N11",n11);
		o11.getSettoriAdiacenti().put("O10",o10);
		o11.getSettoriAdiacenti().put("O12",o12);
		o11.getSettoriAdiacenti().put("P10",p10);
		o11.getSettoriAdiacenti().put("P11",p11);
		
		o12.getSettoriAdiacenti().put("N11",n11);
		o12.getSettoriAdiacenti().put("N12",n12);
		o12.getSettoriAdiacenti().put("O11",o11);
		o12.getSettoriAdiacenti().put("O13",o13);
		o12.getSettoriAdiacenti().put("P11",p11);
		o12.getSettoriAdiacenti().put("P12",p12);
		
		o13.getSettoriAdiacenti().put("N12",n12);
		o13.getSettoriAdiacenti().put("N13",n13);
		o13.getSettoriAdiacenti().put("O12",o12);
		o13.getSettoriAdiacenti().put("O14",o14);
		o13.getSettoriAdiacenti().put("P12",p12);
		o13.getSettoriAdiacenti().put("P13",p13);
		
		o14.getSettoriAdiacenti().put("N13",n13);
		o14.getSettoriAdiacenti().put("N14",n14);
		o14.getSettoriAdiacenti().put("O13",o13);
		o14.getSettoriAdiacenti().put("P13",p13);
		o14.getSettoriAdiacenti().put("P14",p14);

		
		//adiacenti P
		p1.getSettoriAdiacenti().put("O02",o2);
		p1.getSettoriAdiacenti().put("P02",p2);
		p1.getSettoriAdiacenti().put("Q01",q1);
		p1.getSettoriAdiacenti().put("Q02",q2);
		
		p2.getSettoriAdiacenti().put("O02",o2);
		p2.getSettoriAdiacenti().put("P01",p1);
		p2.getSettoriAdiacenti().put("P03",p3);
		p2.getSettoriAdiacenti().put("Q02",q2);
		p2.getSettoriAdiacenti().put("Q03",q3);

		p3.getSettoriAdiacenti().put("P02",p2);
		p3.getSettoriAdiacenti().put("P04",p4);
		p3.getSettoriAdiacenti().put("Q03",q3);
		p3.getSettoriAdiacenti().put("Q04",q4);

		p4.getSettoriAdiacenti().put("O05",o5);
		p4.getSettoriAdiacenti().put("P03",p3);
		p4.getSettoriAdiacenti().put("P05",p5);
		p4.getSettoriAdiacenti().put("Q04",q4);
		p4.getSettoriAdiacenti().put("Q05",q5);
		
		p5.getSettoriAdiacenti().put("O05",o5);
		p5.getSettoriAdiacenti().put("O06",o6);
		p5.getSettoriAdiacenti().put("P04",p4);
		p5.getSettoriAdiacenti().put("P06",p6);
		p5.getSettoriAdiacenti().put("Q05",q5);
		p5.getSettoriAdiacenti().put("Q06",q6);
		
		p6.getSettoriAdiacenti().put("O06",o6);
		p6.getSettoriAdiacenti().put("O07",o7);
		p6.getSettoriAdiacenti().put("P05",p5);
		p6.getSettoriAdiacenti().put("P07",p7);
		p6.getSettoriAdiacenti().put("Q06",q6);
		p6.getSettoriAdiacenti().put("Q07",q7);
		
		p7.getSettoriAdiacenti().put("O07",o7);
		p7.getSettoriAdiacenti().put("O08",o8);
		p7.getSettoriAdiacenti().put("P06",p6);
		p7.getSettoriAdiacenti().put("P08",p8);
		p7.getSettoriAdiacenti().put("Q07",q7);
		p7.getSettoriAdiacenti().put("Q08",q8);
		
		p8.getSettoriAdiacenti().put("O08",o8);
		p8.getSettoriAdiacenti().put("O09",o9);
		p8.getSettoriAdiacenti().put("P07",p7);
		p8.getSettoriAdiacenti().put("P09",p9);
		p8.getSettoriAdiacenti().put("Q08",q8);
		p8.getSettoriAdiacenti().put("Q09",q9);
		
		p9.getSettoriAdiacenti().put("O09",o9);
		p9.getSettoriAdiacenti().put("O10",o10);
		p9.getSettoriAdiacenti().put("P08",p8);
		p9.getSettoriAdiacenti().put("P10",p10);
		p9.getSettoriAdiacenti().put("Q09",q9);
		p9.getSettoriAdiacenti().put("Q10",q10);
		
		p10.getSettoriAdiacenti().put("O10",o10);
		p10.getSettoriAdiacenti().put("O11",o11);
		p10.getSettoriAdiacenti().put("P09",p9);
		p10.getSettoriAdiacenti().put("P11",p11);
		p10.getSettoriAdiacenti().put("Q10",q10);
		p10.getSettoriAdiacenti().put("Q11",q11);
		
		p11.getSettoriAdiacenti().put("O11",o11);
		p11.getSettoriAdiacenti().put("O12",o12);
		p11.getSettoriAdiacenti().put("P10",p10);
		p11.getSettoriAdiacenti().put("P12",p12);
		p11.getSettoriAdiacenti().put("Q11",q11);
		p11.getSettoriAdiacenti().put("Q12",q12);
		
		p12.getSettoriAdiacenti().put("O12",o12);
		p12.getSettoriAdiacenti().put("O13",o13);
		p12.getSettoriAdiacenti().put("P11",p11);
		p12.getSettoriAdiacenti().put("P13",p13);
		p12.getSettoriAdiacenti().put("Q12",q12);
		p12.getSettoriAdiacenti().put("Q13",q13);
		
		p13.getSettoriAdiacenti().put("O13",o13);
		p13.getSettoriAdiacenti().put("O14",o14);
		p13.getSettoriAdiacenti().put("P12",p12);
		p13.getSettoriAdiacenti().put("P14",p14);
		p13.getSettoriAdiacenti().put("Q13",q13);
		p13.getSettoriAdiacenti().put("Q14",q14);
		
		p14.getSettoriAdiacenti().put("O14",o14);
		p14.getSettoriAdiacenti().put("P13",p13);
		p14.getSettoriAdiacenti().put("Q14",q14);
		
		//adiacenti Q
		q1.getSettoriAdiacenti().put("P01",p1);
		q1.getSettoriAdiacenti().put("Q02",q2);
		q1.getSettoriAdiacenti().put("R01",r1);
		
		q2.getSettoriAdiacenti().put("P01",p1);
		q2.getSettoriAdiacenti().put("P02",p2);
		q2.getSettoriAdiacenti().put("Q01",q1);
		q2.getSettoriAdiacenti().put("Q03",q3);
		q2.getSettoriAdiacenti().put("R01",r1);
		q2.getSettoriAdiacenti().put("R02",r2);
		
		q3.getSettoriAdiacenti().put("P02",p2);
		q3.getSettoriAdiacenti().put("P03",p3);
		q3.getSettoriAdiacenti().put("Q02",q2);
		q3.getSettoriAdiacenti().put("Q04",q4);
		q3.getSettoriAdiacenti().put("R02",r2);
		q3.getSettoriAdiacenti().put("R03",r3);
		
		q4.getSettoriAdiacenti().put("P03",p3);
		q4.getSettoriAdiacenti().put("P04",p4);
		q4.getSettoriAdiacenti().put("Q03",q3);
		q4.getSettoriAdiacenti().put("Q05",q5);
		q4.getSettoriAdiacenti().put("R03",r3);
		q4.getSettoriAdiacenti().put("R04",r4);
		
		q5.getSettoriAdiacenti().put("P04",p4);
		q5.getSettoriAdiacenti().put("P05",p5);
		q5.getSettoriAdiacenti().put("Q04",q4);
		q5.getSettoriAdiacenti().put("Q06",q6);
		q5.getSettoriAdiacenti().put("R04",r4);
		q5.getSettoriAdiacenti().put("R05",r5);
		
		q6.getSettoriAdiacenti().put("P05",p5);
		q6.getSettoriAdiacenti().put("P06",p6);
		q6.getSettoriAdiacenti().put("Q05",q5);
		q6.getSettoriAdiacenti().put("Q07",q7);
		q6.getSettoriAdiacenti().put("R05",r5);
		q6.getSettoriAdiacenti().put("R06",r6);
		
		q7.getSettoriAdiacenti().put("P06",p6);
		q7.getSettoriAdiacenti().put("P07",p7);
		q7.getSettoriAdiacenti().put("Q06",q6);
		q7.getSettoriAdiacenti().put("Q08",q8);
		q7.getSettoriAdiacenti().put("R06",r6);
		q7.getSettoriAdiacenti().put("R07",r7);
		
		q8.getSettoriAdiacenti().put("P07",p7);
		q8.getSettoriAdiacenti().put("P08",p8);
		q8.getSettoriAdiacenti().put("Q07",q7);
		q8.getSettoriAdiacenti().put("Q09",q9);
		q8.getSettoriAdiacenti().put("R07",r7);
		q8.getSettoriAdiacenti().put("R08",r8);
		
		q9.getSettoriAdiacenti().put("P08",p8);
		q9.getSettoriAdiacenti().put("P09",p9);
		q9.getSettoriAdiacenti().put("Q08",q8);
		q9.getSettoriAdiacenti().put("Q10",q10);
		q9.getSettoriAdiacenti().put("R08",r8);
		q9.getSettoriAdiacenti().put("R09",r9);
		
		q10.getSettoriAdiacenti().put("P09",p9);
		q10.getSettoriAdiacenti().put("P10",p10);
		q10.getSettoriAdiacenti().put("Q09",q9);
		q10.getSettoriAdiacenti().put("Q11",q11);
		q10.getSettoriAdiacenti().put("R09",r9);

		q11.getSettoriAdiacenti().put("P10",p10);
		q11.getSettoriAdiacenti().put("P11",p11);
		q11.getSettoriAdiacenti().put("Q10",q10);
		q11.getSettoriAdiacenti().put("Q12",q12);

		q12.getSettoriAdiacenti().put("P11",p11);
		q12.getSettoriAdiacenti().put("P12",p12);
		q12.getSettoriAdiacenti().put("Q11",q11);
		q12.getSettoriAdiacenti().put("Q13",q13);
		q12.getSettoriAdiacenti().put("R12",r12);
		
		q13.getSettoriAdiacenti().put("P12",p12);
		q13.getSettoriAdiacenti().put("P13",p13);
		q13.getSettoriAdiacenti().put("Q12",q12);
		q13.getSettoriAdiacenti().put("Q14",q14);
		q13.getSettoriAdiacenti().put("R12",r12);
		q13.getSettoriAdiacenti().put("R13",r13);
		
		q14.getSettoriAdiacenti().put("P13",p13);
		q14.getSettoriAdiacenti().put("P14",p14);
		q14.getSettoriAdiacenti().put("Q13",q13);
		q14.getSettoriAdiacenti().put("R13",r13);
		
		//adiacenti R
		r1.getSettoriAdiacenti().put("Q01",q1);
		r1.getSettoriAdiacenti().put("Q02",q2);
		r1.getSettoriAdiacenti().put("R02",r2);
		r1.getSettoriAdiacenti().put("S02",s2);
		
		r2.getSettoriAdiacenti().put("Q02",q2);
		r2.getSettoriAdiacenti().put("Q03",q3);
		r2.getSettoriAdiacenti().put("R01",r1);
		r2.getSettoriAdiacenti().put("R03",r3);
		r2.getSettoriAdiacenti().put("S02",s2);

		r3.getSettoriAdiacenti().put("Q03",q3);
		r3.getSettoriAdiacenti().put("Q04",q4);
		r3.getSettoriAdiacenti().put("R02",r2);
		r3.getSettoriAdiacenti().put("R04",r4);
		r3.getSettoriAdiacenti().put("S04",s4);
		
		r4.getSettoriAdiacenti().put("Q04",q4);
		r4.getSettoriAdiacenti().put("Q05",q5);
		r4.getSettoriAdiacenti().put("R03",r3);
		r4.getSettoriAdiacenti().put("R05",r5);
		r4.getSettoriAdiacenti().put("S04",s4);
		r4.getSettoriAdiacenti().put("S05",s5);
		
		r5.getSettoriAdiacenti().put("Q05",q5);
		r5.getSettoriAdiacenti().put("Q06",q6);
		r5.getSettoriAdiacenti().put("R04",r4);
		r5.getSettoriAdiacenti().put("R06",r6);
		r5.getSettoriAdiacenti().put("S05",s5);
		r5.getSettoriAdiacenti().put("S06",s6);
		
		r6.getSettoriAdiacenti().put("Q06",q6);
		r6.getSettoriAdiacenti().put("Q07",q7);
		r6.getSettoriAdiacenti().put("R05",r5);
		r6.getSettoriAdiacenti().put("R07",r7);
		r6.getSettoriAdiacenti().put("S06",s6);
		r6.getSettoriAdiacenti().put("S07",s7);
		
		r7.getSettoriAdiacenti().put("Q07",q7);
		r7.getSettoriAdiacenti().put("Q08",q8);
		r7.getSettoriAdiacenti().put("R06",r6);
		r7.getSettoriAdiacenti().put("R08",r8);
		r7.getSettoriAdiacenti().put("S07",s7);
		r7.getSettoriAdiacenti().put("S08",s8);
		
		r8.getSettoriAdiacenti().put("Q08",q8);
		r8.getSettoriAdiacenti().put("Q09",q9);
		r8.getSettoriAdiacenti().put("R07",r7);
		r8.getSettoriAdiacenti().put("R09",r9);
		r8.getSettoriAdiacenti().put("S08",s8);
		r8.getSettoriAdiacenti().put("S09",s9);
		
		r9.getSettoriAdiacenti().put("Q09",q9);
		r9.getSettoriAdiacenti().put("Q10",q10);
		r9.getSettoriAdiacenti().put("R08",r8);
		r9.getSettoriAdiacenti().put("S09",s9);

		r12.getSettoriAdiacenti().put("Q12",q12);
		r12.getSettoriAdiacenti().put("Q13",q13);
		r12.getSettoriAdiacenti().put("R13",r13);
		r12.getSettoriAdiacenti().put("S12",s12);
		r12.getSettoriAdiacenti().put("S13",s13);
		
		r13.getSettoriAdiacenti().put("Q13",q13);
		r13.getSettoriAdiacenti().put("Q14",q14);
		r13.getSettoriAdiacenti().put("R12",r12);
		r13.getSettoriAdiacenti().put("S13",s13);
		
		//adiacenti S
		s2.getSettoriAdiacenti().put("R01",r1);
		s2.getSettoriAdiacenti().put("R02",r2);
		s2.getSettoriAdiacenti().put("T02",t2);

		s4.getSettoriAdiacenti().put("R03",r3);
		s4.getSettoriAdiacenti().put("R04",r4);
		s4.getSettoriAdiacenti().put("S05",s5);

		s5.getSettoriAdiacenti().put("R04",r4);
		s5.getSettoriAdiacenti().put("R05",r5);
		s5.getSettoriAdiacenti().put("S04",s4);
		s5.getSettoriAdiacenti().put("S06",s6);
		s5.getSettoriAdiacenti().put("T05",t5);
		
		s6.getSettoriAdiacenti().put("R05",r5);
		s6.getSettoriAdiacenti().put("R06",r6);
		s6.getSettoriAdiacenti().put("S05",s5);
		s6.getSettoriAdiacenti().put("S07",s7);
		s6.getSettoriAdiacenti().put("T05",t5);
		s6.getSettoriAdiacenti().put("T06",t6);
		
		s7.getSettoriAdiacenti().put("R06",r6);
		s7.getSettoriAdiacenti().put("R07",r7);
		s7.getSettoriAdiacenti().put("S06",s6);
		s7.getSettoriAdiacenti().put("S08",s8);
		s7.getSettoriAdiacenti().put("T06",t6);
		s7.getSettoriAdiacenti().put("T07",t7);
		
		s8.getSettoriAdiacenti().put("R07",r7);
		s8.getSettoriAdiacenti().put("R08",r8);
		s8.getSettoriAdiacenti().put("S07",s7);
		s8.getSettoriAdiacenti().put("S09",s9);
		s8.getSettoriAdiacenti().put("T07",t7);
		s8.getSettoriAdiacenti().put("T08",t8);
		
		s9.getSettoriAdiacenti().put("R08",r8);
		s9.getSettoriAdiacenti().put("R09",r9);
		s9.getSettoriAdiacenti().put("S08",s8);
		s9.getSettoriAdiacenti().put("T08",t8);

		s12.getSettoriAdiacenti().put("R12",r12);
		s12.getSettoriAdiacenti().put("S13",s13);
		s12.getSettoriAdiacenti().put("T11",t11);
		s12.getSettoriAdiacenti().put("T12",t12);
		
		s13.getSettoriAdiacenti().put("R12",r12);
		s13.getSettoriAdiacenti().put("R13",r13);
		s13.getSettoriAdiacenti().put("S12",s12);
		s13.getSettoriAdiacenti().put("T12",t12);
		s13.getSettoriAdiacenti().put("T13",t13);
		
		
		//adiacenti T
		t2.getSettoriAdiacenti().put("S02",s2);
		t2.getSettoriAdiacenti().put("U02",u2);
		t2.getSettoriAdiacenti().put("U03",u3);
		
		t5.getSettoriAdiacenti().put("S05",s5);
		t5.getSettoriAdiacenti().put("S06",s6);
		t5.getSettoriAdiacenti().put("T06",t6);
		t5.getSettoriAdiacenti().put("U05",u5);
		t5.getSettoriAdiacenti().put("U06",u6);
		
		t6.getSettoriAdiacenti().put("S06",s6);
		t6.getSettoriAdiacenti().put("S07",s7);
		t6.getSettoriAdiacenti().put("T05",t5);
		t6.getSettoriAdiacenti().put("T07",t7);
		t6.getSettoriAdiacenti().put("U06",u6);
		t6.getSettoriAdiacenti().put("U07",u7);
		
		t7.getSettoriAdiacenti().put("S07",s7);
		t7.getSettoriAdiacenti().put("S08",s8);
		t7.getSettoriAdiacenti().put("T06",t6);
		t7.getSettoriAdiacenti().put("T08",t8);
		t7.getSettoriAdiacenti().put("U07",u7);
		t7.getSettoriAdiacenti().put("U08",u8);
		
		t8.getSettoriAdiacenti().put("S08",s8);
		t8.getSettoriAdiacenti().put("S09",s9);
		t8.getSettoriAdiacenti().put("T07",t7);
		t8.getSettoriAdiacenti().put("U08",u8);
		t8.getSettoriAdiacenti().put("U09",u9);
		
		t11.getSettoriAdiacenti().put("S12",s12);
		t11.getSettoriAdiacenti().put("T12",t12);
		t11.getSettoriAdiacenti().put("U11",u11);
		t11.getSettoriAdiacenti().put("U12",u12);
		
		t12.getSettoriAdiacenti().put("S12",s12);
		t12.getSettoriAdiacenti().put("S13",s13);
		t12.getSettoriAdiacenti().put("T11",t11);
		t12.getSettoriAdiacenti().put("T13",t13);
		t12.getSettoriAdiacenti().put("U12",u12);
		t12.getSettoriAdiacenti().put("U13",u13);
		
		t13.getSettoriAdiacenti().put("S13",s13);
		t13.getSettoriAdiacenti().put("T12",t12);
		t13.getSettoriAdiacenti().put("T14",t14);
		t13.getSettoriAdiacenti().put("U13",u13);
		t13.getSettoriAdiacenti().put("U14",u14);
		
		t14.getSettoriAdiacenti().put("T13",t13);
		t14.getSettoriAdiacenti().put("U14",u14);
		
		//adiacenti U
		u1.getSettoriAdiacenti().put("U02",u2);
		u1.getSettoriAdiacenti().put("V01",v1);

		u2.getSettoriAdiacenti().put("T02",t2);
		u2.getSettoriAdiacenti().put("U01",u1);
		u2.getSettoriAdiacenti().put("U03",u3);
		u2.getSettoriAdiacenti().put("V01",v1);
		u2.getSettoriAdiacenti().put("Z02",scialuppa2);
		
		u3.getSettoriAdiacenti().put("T02",t2);
		u3.getSettoriAdiacenti().put("U02",u2);
		u3.getSettoriAdiacenti().put("U04",u4);
		u3.getSettoriAdiacenti().put("Z02",scialuppa2);
		u3.getSettoriAdiacenti().put("V03",v3);
		
		u4.getSettoriAdiacenti().put("U03",u3);
		u4.getSettoriAdiacenti().put("U05",u5);
		u4.getSettoriAdiacenti().put("V03",v3);
		u4.getSettoriAdiacenti().put("V04",v4);

		u5.getSettoriAdiacenti().put("T05",t5);
		u5.getSettoriAdiacenti().put("U04",u4);
		u5.getSettoriAdiacenti().put("U06",u6);
		u5.getSettoriAdiacenti().put("V04",v4);
		u5.getSettoriAdiacenti().put("V05",v5);
		
		u6.getSettoriAdiacenti().put("T05",t5);
		u6.getSettoriAdiacenti().put("T06",t6);
		u6.getSettoriAdiacenti().put("U05",u5);
		u6.getSettoriAdiacenti().put("U07",u7);
		u6.getSettoriAdiacenti().put("V05",v5);
		u6.getSettoriAdiacenti().put("V06",v6);
		
		u7.getSettoriAdiacenti().put("T06",t6);
		u7.getSettoriAdiacenti().put("T07",t7);
		u7.getSettoriAdiacenti().put("U06",u6);
		u7.getSettoriAdiacenti().put("U08",u8);
		u7.getSettoriAdiacenti().put("V06",v6);

		u8.getSettoriAdiacenti().put("T07",t7);
		u8.getSettoriAdiacenti().put("T08",t8);
		u8.getSettoriAdiacenti().put("U07",u7);
		u8.getSettoriAdiacenti().put("U09",u9);
		u8.getSettoriAdiacenti().put("V08",v8);
		
		u9.getSettoriAdiacenti().put("T08",t8);
		u9.getSettoriAdiacenti().put("U08",u8);
		u9.getSettoriAdiacenti().put("U10",u10);
		u9.getSettoriAdiacenti().put("V08",v8);
		u9.getSettoriAdiacenti().put("V09",v9);
		
		u10.getSettoriAdiacenti().put("U09",u9);
		u10.getSettoriAdiacenti().put("U11",u11);
		u10.getSettoriAdiacenti().put("V09",v9);
		u10.getSettoriAdiacenti().put("V10",v10);

		u11.getSettoriAdiacenti().put("T11",t11);
		u11.getSettoriAdiacenti().put("U10",u10);
		u11.getSettoriAdiacenti().put("U12",u12);
		u11.getSettoriAdiacenti().put("V10",v10);
		u11.getSettoriAdiacenti().put("V11",v11);
		
		u12.getSettoriAdiacenti().put("T11",t11);
		u12.getSettoriAdiacenti().put("T12",t12);
		u12.getSettoriAdiacenti().put("U11",u11);
		u12.getSettoriAdiacenti().put("U13",u13);
		u12.getSettoriAdiacenti().put("V11",v11);
		u12.getSettoriAdiacenti().put("V12",v12);
		
		u13.getSettoriAdiacenti().put("T12",t12);
		u13.getSettoriAdiacenti().put("T13",t13);
		u13.getSettoriAdiacenti().put("U12",u12);
		u13.getSettoriAdiacenti().put("U14",u14);
		u13.getSettoriAdiacenti().put("V12",v12);
		u13.getSettoriAdiacenti().put("Z03",scialuppa3);
		
		u14.getSettoriAdiacenti().put("T13",t13);
		u14.getSettoriAdiacenti().put("T14",t14);
		u14.getSettoriAdiacenti().put("U13",u13);
		u14.getSettoriAdiacenti().put("Z03",scialuppa3);
		u14.getSettoriAdiacenti().put("V14",v14);

		
		//adiacenti V
		v1.getSettoriAdiacenti().put("U01",u1);
		v1.getSettoriAdiacenti().put("U02",u2);
		v1.getSettoriAdiacenti().put("Z02",scialuppa2);
		v1.getSettoriAdiacenti().put("W02",w2);
		
		v3.getSettoriAdiacenti().put("U03",u3);
		v3.getSettoriAdiacenti().put("U04",u4);
		v3.getSettoriAdiacenti().put("Z02",scialuppa2);
		v3.getSettoriAdiacenti().put("V04",v4);
		v3.getSettoriAdiacenti().put("W03",w3);
		v3.getSettoriAdiacenti().put("W04",w4);
		
		v4.getSettoriAdiacenti().put("U04",u4);
		v4.getSettoriAdiacenti().put("U05",u5);
		v4.getSettoriAdiacenti().put("V03",v3);
		v4.getSettoriAdiacenti().put("V05",v5);
		v4.getSettoriAdiacenti().put("W04",w4);
		v4.getSettoriAdiacenti().put("W05",w5);
		
		v5.getSettoriAdiacenti().put("U05",u5);
		v5.getSettoriAdiacenti().put("U06",u6);
		v5.getSettoriAdiacenti().put("V04",v4);
		v5.getSettoriAdiacenti().put("V06",v6);
		v5.getSettoriAdiacenti().put("W05",w5);
		v5.getSettoriAdiacenti().put("W06",w6);
		
		v6.getSettoriAdiacenti().put("U06",u6);
		v6.getSettoriAdiacenti().put("U07",u7);
		v6.getSettoriAdiacenti().put("V05",v5);
		v6.getSettoriAdiacenti().put("W06",w6);
		
		v8.getSettoriAdiacenti().put("U08",u8);
		v8.getSettoriAdiacenti().put("U09",u9);
		v8.getSettoriAdiacenti().put("V09",v9);
		v8.getSettoriAdiacenti().put("W09",w9);
		
		v9.getSettoriAdiacenti().put("U09",u9);
		v9.getSettoriAdiacenti().put("U10",u10);
		v9.getSettoriAdiacenti().put("V08",v8);
		v9.getSettoriAdiacenti().put("V10",v10);
		v9.getSettoriAdiacenti().put("W09",w9);
		v9.getSettoriAdiacenti().put("W10",w10);
		
		v10.getSettoriAdiacenti().put("U10",u10);
		v10.getSettoriAdiacenti().put("U11",u11);
		v10.getSettoriAdiacenti().put("V09",v9);
		v10.getSettoriAdiacenti().put("V11",v11);
		v10.getSettoriAdiacenti().put("W10",w10);
		v10.getSettoriAdiacenti().put("W11",w11);
		
		v11.getSettoriAdiacenti().put("U11",u11);
		v11.getSettoriAdiacenti().put("U12",u12);
		v11.getSettoriAdiacenti().put("V10",v10);
		v11.getSettoriAdiacenti().put("V12",v12);
		v11.getSettoriAdiacenti().put("W11",w11);
		v11.getSettoriAdiacenti().put("W12",w12);
		
		v12.getSettoriAdiacenti().put("U12",u12);
		v12.getSettoriAdiacenti().put("U13",u13);
		v12.getSettoriAdiacenti().put("V11",v11);
		v12.getSettoriAdiacenti().put("Z03",scialuppa3);
		v12.getSettoriAdiacenti().put("W12",w12);
		v12.getSettoriAdiacenti().put("W13",w13);

		v14.getSettoriAdiacenti().put("U14",u14);
		v14.getSettoriAdiacenti().put("Z03",scialuppa3);
		v14.getSettoriAdiacenti().put("W14",w14);

		//adiacenti W
		w2.getSettoriAdiacenti().put("V01",v1);
		w2.getSettoriAdiacenti().put("Z02",scialuppa2);
		w2.getSettoriAdiacenti().put("W03",w3);

		w3.getSettoriAdiacenti().put("Z02",scialuppa2);
		w3.getSettoriAdiacenti().put("V03",v3);
		w3.getSettoriAdiacenti().put("W02",w2);
		w3.getSettoriAdiacenti().put("W04",w4);

		w4.getSettoriAdiacenti().put("V03",v3);
		w4.getSettoriAdiacenti().put("V04",v4);
		w4.getSettoriAdiacenti().put("W03",w3);
		w4.getSettoriAdiacenti().put("W05",w5);

		w5.getSettoriAdiacenti().put("V04",v4);
		w5.getSettoriAdiacenti().put("V05",v5);
		w5.getSettoriAdiacenti().put("W04",w4);
		w5.getSettoriAdiacenti().put("W06",w6);

		w6.getSettoriAdiacenti().put("V05",v5);
		w6.getSettoriAdiacenti().put("V06",v6);
		w6.getSettoriAdiacenti().put("W05",w5);

		w9.getSettoriAdiacenti().put("V08",v8);
		w9.getSettoriAdiacenti().put("V09",v9);
		w9.getSettoriAdiacenti().put("W10",w10);

		w10.getSettoriAdiacenti().put("V09",v9);
		w10.getSettoriAdiacenti().put("V10",v10);
		w10.getSettoriAdiacenti().put("W09",w9);
		w10.getSettoriAdiacenti().put("W11",w11);

		w11.getSettoriAdiacenti().put("V10",v10);
		w11.getSettoriAdiacenti().put("V11",v11);
		w11.getSettoriAdiacenti().put("W10",w10);
		w11.getSettoriAdiacenti().put("W12",w12);

		w12.getSettoriAdiacenti().put("V11",v11);
		w12.getSettoriAdiacenti().put("V12",v12);
		w12.getSettoriAdiacenti().put("W11",w11);
		w12.getSettoriAdiacenti().put("W13",w13);

		w13.getSettoriAdiacenti().put("V12",v12);
		w13.getSettoriAdiacenti().put("Z03",scialuppa3);
		w13.getSettoriAdiacenti().put("W12",w12);
		w13.getSettoriAdiacenti().put("W14",w14);

		w14.getSettoriAdiacenti().put("Z03",scialuppa3);
		w14.getSettoriAdiacenti().put("V14",v14);
		w14.getSettoriAdiacenti().put("W13",w13);
		
//		//carte scialuppa
//		CartaScialuppa carta=null;
//		for(Integer i=0; i<Costanti.NCARTESCIALUPPA;i++){
//			if(i<Costanti.NCARTESCIALUPPA-2){
//				carta=new CartaScialuppa(i.toString(),Colore.VERDE);
//			}else{
//				carta=new CartaScialuppa(i.toString(),Colore.ROSSO);
//			}
//			getCarteScialuppa().put(i.toString(), carta);
//		}
//		
//		//carte settore
//		//5 per tipo
//		CartaSettore cartaSettore=null;
//		for(Integer j=0;j<Costanti.NCARTESETTORE;j++){
//			if (j<(Costanti.NCARTESETTORE/5)){
//				cartaSettore=new CartaSettore(j.toString(), CSettore.REAL, true);
//			}else if(j>=(Costanti.NCARTESETTORE/5) && j<((Costanti.NCARTESETTORE/5)*2)){
//				cartaSettore=new CartaSettore(j.toString(), CSettore.REAL, false);
//			}else if(j>=((Costanti.NCARTESETTORE/5)*2) && j<((Costanti.NCARTESETTORE/5)*3)){
//				cartaSettore=new CartaSettore(j.toString(), CSettore.FAKE, false);
//			}else if(j>=((Costanti.NCARTESETTORE/5)*3) && j<((Costanti.NCARTESETTORE/5)*4)){
//				cartaSettore=new CartaSettore(j.toString(), CSettore.FAKE, true);
//			}else if(j>=((Costanti.NCARTESETTORE/5)*4) && j<Costanti.NCARTESETTORE){
//				cartaSettore=new CartaSettore(j.toString(), CSettore.SILENCE, false);
//			}
//			getCarteSettore().put(j.toString(), cartaSettore);
//		}
//		



	}

	public Map<String, Casella> getSettori() {
		return settori;
	}

	public void setSettori(Map<String, Casella> settori) {
		this.settori = settori;
	}

//	public Map<String, CartaScialuppa> getCarteScialuppa() {
//		return carteScialuppa;
//	}
//
//	public void setCarteScialuppa(Map<String, CartaScialuppa> carteScialuppa) {
//		this.carteScialuppa = carteScialuppa;
//	}
//
//	public Map<String, CartaSettore> getCarteSettore() {
//		return carteSettore;
//	}
//
//	public void setCarteSettore(Map<String, CartaSettore> carteSettore) {
//		this.carteSettore = carteSettore;
//	}
}
