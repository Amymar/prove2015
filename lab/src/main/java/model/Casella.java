package model;

import java.util.Map;
import java.util.TreeMap;

import model.Enumerazioni.*;

public class Casella {
	private String IdUnivoco ;
	private TipoTerreno tipo;
	private Map <String, Casella> settoriAdiacenti;
	
	public Casella(String nome, TipoTerreno tipo){
		this.IdUnivoco=nome;
		this.tipo=tipo;
		this.settoriAdiacenti=new TreeMap <String, Casella> ();
	}
	
	public static Casella getCasella(String IdUnivoco, TipoTerreno tipo, Map <String, Casella> caselle){
		if(caselle.containsKey(IdUnivoco)){
			return caselle.get(IdUnivoco);
		}else{
			Casella casella = new Casella(IdUnivoco,tipo);
			caselle.put(IdUnivoco, casella);
			return casella;
		}
		
	}
	
	public Map<String, Casella> getSettoriAdiacenti() {
		return settoriAdiacenti;
	}


	public void setSettoriAdiacenti(Map<String, Casella> settoriAdiacenti) {
		this.settoriAdiacenti = settoriAdiacenti;
	}

	public String getNome() {
		return IdUnivoco;
	}
	

	public void setNome(String nome) {
		this.IdUnivoco = nome;
	}
	public TipoTerreno getTipo() {
		return tipo;
	}
	public void setTipo(TipoTerreno tipo) {
		this.tipo = tipo;
	}
}
