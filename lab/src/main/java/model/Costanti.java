package model;

public class Costanti {
	
	public final static int NCARTESCIALUPPA =6;
	public final static int NCARTESETTORE =25;
	public final static int NCARTEOGGETTO=12;
	public final static int NTIPOOGGETTO=6;
	public final static int NTIPOCARTASETTORE=5;
	public final static int NSCIALUPPE= 4;
	public final static int NMAXOGGETTI=3;
	public final static int NMAXTURNI=39;
}
