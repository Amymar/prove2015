package model;

import model.Enumerazioni.*;

public class CartaOggetto {

	private int id;
	private COggetto tipo;
	
	public CartaOggetto (int id, COggetto tipo){
		this.id=id;
		this.tipo=tipo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public COggetto getTipo() {
		return tipo;
	}

	public void setTipo(COggetto tipo) {
		this.tipo = tipo;
	}
	
	
}
