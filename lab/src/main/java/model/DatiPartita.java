package model;

import java.util.Map;
import java.util.TreeMap;

import model.Enumerazioni.*;

public class DatiPartita {

	private Mappa mappa;
	private Map <String,CartaSettore> carteSettore= new TreeMap <String,CartaSettore>();
	private Map<String, CartaOggetto> carteOggetto= new TreeMap <String, CartaOggetto>();
	private Map <String, CartaScialuppa> carteScialuppa= new TreeMap <String, CartaScialuppa> ();
	private Map <String, CartaPersonaggio> umani=new TreeMap <String, CartaPersonaggio>();
	private Map <String, CartaPersonaggio> alieni=new TreeMap <String, CartaPersonaggio>();

	public DatiPartita(){

		//inizzializzo mappa
		this.mappa=new Mappa();

		creoCarteSettore();
		creoCarteOggetto();
		creoCarteScialuppa();
		creoCartePersonaggio();
	}

		//inizzializzo Carte settore
	private void creoCarteSettore(){
		CartaSettore cartaSettore=null;
		for(Integer j=0;j<Costanti.NCARTESETTORE;j++){
			if (j<(Costanti.NCARTESETTORE/Costanti.NTIPOCARTASETTORE)){
				cartaSettore=new CartaSettore(j.toString(), CSettore.REAL, true);
			}else if(j>=(Costanti.NCARTESETTORE/Costanti.NTIPOCARTASETTORE) && j<((Costanti.NCARTESETTORE/Costanti.NTIPOCARTASETTORE)*2)){
				cartaSettore=new CartaSettore(j.toString(), CSettore.REAL, false);
			}else if(j>=((Costanti.NCARTESETTORE/Costanti.NTIPOCARTASETTORE)*2) && j<((Costanti.NCARTESETTORE/Costanti.NTIPOCARTASETTORE)*3)){
				cartaSettore=new CartaSettore(j.toString(), CSettore.FAKE, false);
			}else if(j>=((Costanti.NCARTESETTORE/Costanti.NTIPOCARTASETTORE)*3) && j<((Costanti.NCARTESETTORE/Costanti.NTIPOCARTASETTORE)*4)){
				cartaSettore=new CartaSettore(j.toString(), CSettore.FAKE, true);
			}else if(j>=((Costanti.NCARTESETTORE/Costanti.NTIPOCARTASETTORE)*4) && j<Costanti.NCARTESETTORE){
				cartaSettore=new CartaSettore(j.toString(), CSettore.SILENCE, false);
			}
			getCarteSettore().put(j.toString(), cartaSettore);
		}
	}


		//inizzializzo carte oggetto
	private void creoCarteOggetto(){
		CartaOggetto cartaOggetto=null;
		for(Integer i=0;i<Costanti.NCARTEOGGETTO;i++){
			if (i<(Costanti.NCARTEOGGETTO/Costanti.NTIPOOGGETTO)){
				cartaOggetto=new CartaOggetto(i, COggetto.ATTACCO);
			}else if(i>=(Costanti.NCARTEOGGETTO/Costanti.NTIPOOGGETTO) && i<((Costanti.NCARTEOGGETTO/Costanti.NTIPOOGGETTO)*2)){
				cartaOggetto=new CartaOggetto(i, COggetto.TELETRASPORTO);
			}else if(i>=((Costanti.NCARTEOGGETTO/Costanti.NTIPOOGGETTO)*2) && i<((Costanti.NCARTEOGGETTO/Costanti.NTIPOOGGETTO)*3)){
				cartaOggetto=new CartaOggetto(i, COggetto.ADRENALINA);
			}else if(i>=((Costanti.NCARTEOGGETTO/Costanti.NTIPOOGGETTO)*3) && i<((Costanti.NCARTEOGGETTO/Costanti.NTIPOOGGETTO)*4)){
				cartaOggetto=new CartaOggetto(i, COggetto.SEDATIVI);
			}else if(i>=((Costanti.NCARTEOGGETTO/Costanti.NTIPOOGGETTO)*4) && i<((Costanti.NCARTEOGGETTO/Costanti.NTIPOOGGETTO)*5)){
				cartaOggetto=new CartaOggetto(i, COggetto.LUCI);
			}else if(i>=((Costanti.NCARTEOGGETTO/Costanti.NTIPOOGGETTO)*5) && i<Costanti.NCARTEOGGETTO){
				cartaOggetto=new CartaOggetto(i, COggetto.DIFESA);
			}
			getCarteOggetto().put(i.toString(), cartaOggetto);
		}
	}



		//inizzializzo carte scialuppa
		//assicurandomi che almeno esista la possibilità
		//che in caso di 8 giocatori tutti e quattro gli umani possano scappare
	private void creoCarteScialuppa(){
		CartaScialuppa carta=null;
		for(Integer w=0; w<Costanti.NCARTESCIALUPPA;w++){
			if(w<Costanti.NCARTESCIALUPPA-Costanti.NSCIALUPPE){
				carta=new CartaScialuppa(w.toString(),Colore.ROSSO);
			}else{
				carta=new CartaScialuppa(w.toString(),Colore.VERDE);
			}
			getCarteScialuppa().put(w.toString(), carta);
		}
	}



		//inizzializzo carte personaggio
		// e le posiziono nelle rispettive basi
	private void creoCartePersonaggio(){
		CartaPersonaggio personaggio=null;
		for(Umani per : Enumerazioni.Umani.values()) {
			personaggio=new CartaPersonaggio(per.toString(), Razza.HUMAN,getMappa().getSettori().get("XU"));
			getUmani().put(per.toString(), personaggio);
		}
		for(Enumerazioni.Alieni al: Enumerazioni.Alieni.values()){
			personaggio=new CartaPersonaggio(al.toString(), Razza.ALIEN,getMappa().getSettori().get("XA"));
			getAlieni().put(al.toString(), personaggio);
		}
	}
	public Mappa getMappa() {
		return mappa;
	}
	public void setMappa(Mappa mappa) {
		this.mappa = mappa;
	}
	public Map<String, CartaSettore> getCarteSettore() {
		return carteSettore;
	}
	public void setCarteSettore(Map<String, CartaSettore> carteSettore) {
		this.carteSettore = carteSettore;
	}
	public Map<String, CartaOggetto> getCarteOggetto() {
		return carteOggetto;
	}
	public void setCarteOggetto(Map<String, CartaOggetto> carteOggetto) {
		this.carteOggetto = carteOggetto;
	}
	public Map<String, CartaScialuppa> getCarteScialuppa() {
		return carteScialuppa;
	}
	public void setCarteScialuppa(Map<String, CartaScialuppa> carteScialuppa) {
		this.carteScialuppa = carteScialuppa;
	}
	public Map<String, CartaPersonaggio> getUmani() {
		return umani;
	}
	public void setUmani(Map<String, CartaPersonaggio> umani) {
		this.umani = umani;
	}
	public Map<String, CartaPersonaggio> getAlieni() {
		return alieni;
	}
	public void setAlieni(Map<String, CartaPersonaggio> alieni) {
		this.alieni = alieni;
	}
}


