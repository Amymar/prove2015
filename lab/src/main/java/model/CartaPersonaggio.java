package model;

import java.util.ArrayList;
import java.util.List;

import model.Enumerazioni.*;

public class CartaPersonaggio {

	private String nome;
	private Razza razza;
	private Casella posizione;
	List <String> storicoTurni;
	List<String> oggetti;
	boolean mangiato;
	
	
	public CartaPersonaggio(String nome, Razza razza, Casella posizione){
		this.nome=nome;
		this.razza=razza;
		this.posizione=posizione;
		this.storicoTurni= new ArrayList <String>(Costanti.NMAXTURNI);
		this.oggetti= new ArrayList <String>(Costanti.NMAXOGGETTI);
		this.mangiato=false;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Razza getRazza() {
		return razza;
	}
	public void setRazza(Razza razza) {
		this.razza = razza;
	}
	public Casella getPosizione() {
		return posizione;
	}
	public void setPosizione(Casella posizione) {
		this.posizione = posizione;
	}
	public List<String> getStoricoTurni() {
		return storicoTurni;
	}
	public void setStoricoTurni(List<String> storicoTurni) {
		this.storicoTurni = storicoTurni;
	}
	public List<String> getOggetti() {
		return oggetti;
	}
	public void setOggetti(List<String> oggetti) {
		this.oggetti = oggetti;
	}
	public boolean isMangiato() {
		return mangiato;
	}
	public void setMangiato(boolean mangiato) {
		this.mangiato = mangiato;
	}
}
